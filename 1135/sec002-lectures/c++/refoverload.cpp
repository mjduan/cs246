int foo(int &x){
    x = x + 1;
    return x;
}
int foo(int x){
    x = x + 2;
    return x;
}
int foo(const int x){
    //x = x + 3;
    return x + 3;
}

int foo(const int &x) {
    //x = x + 4;
    return x + 4;
}

int main(){
    int x = 42;
    foo(x);
    return 0;
}
