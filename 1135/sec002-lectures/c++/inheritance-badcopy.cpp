#include <iostream>
using namespace std;

struct Base{
    Base() { cout << "Base()" << endl;}  
    Base(const Base& o){ cout << "Base(Base&)" << endl;}
  //protected:
    Base& operator=(const Base& b){cout <<"Base =" << endl; return *this;}
  
};
struct Derived : public Base{
  int i; // basic type => bitwise copy  
  Derived(){ cout << "Derived()" << endl;}
  Derived(const Derived& d) : i(d.i), Base(d) {
    cout << "Derived(Derived&)" << endl;
  }
  Derived& operator=(const Derived& d){
    i = d.i;
    Base::operator=(d); // cast to base reference - coercion
    cout << "Derived =" << endl;
    return *this;
  }
};

struct Derived2 : public Base{
  double j; // basic type => bitwise copy  
  Derived2(){ cout << "Derived2()" << endl;}
  Derived2(const Derived2& d2) : j(d2.j), Base(d2) {
    cout << "Derived2(Derived2&)" << endl;
  }
  Derived2& operator=(const Derived2& d2){
    j = d2.j;
    Base::operator=(d2); // cast to base reference - coercion
    cout << "Derived2 =" << endl;
    return *this;
  }
};
int main(){
    // Can still do everything with operator=
    Derived a;
    Derived b = a;
    b = a;
    Base c = a ;
    Derived2 d;
    Derived2 e = d;
    d = e;
    

    // Get some polymorphic ptrs
    cout << endl << "Polymorphic ref to two Derived" << endl;
    Base &br1 = *new Derived;
    Base &br2 = *new Derived;
    br1 = br2;
    // Okay, base ptr assignment seems to call Base's operator=

    // Get more polymorphic ptrs
    cout << endl << "Polymorphic ref to  a Derived and a Derived2" << endl;

    Base &d1 = *new Derived;
    Base &d2 = *new Derived2;
    d1 = d2;
    d2 = d1;
    // Uh oh, shouldn't be able to assign subclasses to each other
    // How could we solve this?
}
