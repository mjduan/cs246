#include <iostream>
#include <string>

using namespace std;

int main(){
  string tmp;
  for(;;){
    getline(cin, tmp);
    if( cin.fail() ) break;
    cout << "The entered line was: " << tmp << endl;
  }
}
