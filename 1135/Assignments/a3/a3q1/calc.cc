/**********************************************************************************
/	CS 246: Assignment 3 Part 2
/
/
/
/	Question 1: Calc.cc
/		Implementation of a simple calculator using C++ Objects
/     Supports +, -, *, /, = operators
/     Also supports add to memory (M+), copy from memory (MR) and clear memory (MC)
***********************************************************************************/


#include <iostream>
#include "calc.h"
using namespace std; 

// operator<< overload to support Calc object 
ostream& operator<<(std::ostream &out, const Calc &c){
    if ( c.memory == 0 && c.error == false ){
        cout << c.display;
    
	} else if ( c.memory == 0 && c.error == true ) {
		cout << c.display << " E";
	
	} else if ( c.memory != 0 && c.error == false )  {
		cout << c.display << " M: " << c.memory;
	
	} else if ( c.memory != 0 && c.error == true ) {
		cout << c.display << " M: " << c.memory << " E"; 
	}
}

// default constructor 
Calc::Calc(){ 
 this->error = false;
 this->display = 0;
 this->oper = ' ';
 this->result = 0; 
 this->memory = 0;
}

// copy constructor
Calc::Calc(const Calc &other): error(other.error),
                           display(other.display),
                           oper(other.oper),
                           result(other.result),
                           memory(other.memory) {}


// definition of digit method, adds digit to display from stdin                          
void Calc::digit(int digit){

   if ( digit > 9 or digit < 0) { return; } 
   
   else {      
      this->display = (this->display * 10) + digit;
   }
}


// definition of operator method
void Calc::op(char oper){
	if ( this->oper == ' ' ){
			   
	   this->oper = oper; 
      this->result = this->display;
	    
	} else {
     
	   int temp = 0; // temp variable for holding result 
	   
      if ( this->oper == '+' ){       

			temp = this->result + this->display;
	   }else if ( this->oper == '-' ){ 
      	temp = this->result - this->display;
	
      }else if ( this->oper == '*' ){ 
         temp = this->result * this->display;
	 
	   }else if ( this->oper == '/' ){
			if ( this->display == 0 ) {temp = 0; this->error = true; } 
			else {temp = this->result / this->display;}
		
      }
      
      //cout << "temp is: " << temp << endl;   
      this->result = temp;
	   this->oper = oper;
	   
   }
   this->display = 0; 
}

// function for equals method
void Calc::equals(){
   int temp = 0; 
   
   if ( this->oper == ' ' ) {
      temp = this->display;
   }else if ( this->oper == '+' ){
		temp = this->result + this->display;
	}else if ( this->oper == '-' ) {   	
      temp = this->result - this->display;
   }else if ( this->oper == '*' ) { 
      temp = this->result * this->display;
   }else if ( this->oper == '/' ) { 
      if ( this->display == 0 ) { temp = 0; this->error = true; }
		else {temp = this->result / this->display;}
   }

   this->result = temp; 
   this->display = temp; 

   this->oper = ' ';
}

// function for M+
void Calc::memPlus(){
   this->memory = this->display;
}

// function for MC
void Calc::memClear(){
   this->memory = 0;
}

// function for MR
void Calc::memRecall(){
   this->display = this->memory;
}

// error function
bool Calc::isError() const{
   if (this->error) return true; 
   else return false; 
}

// function for AC
void Calc::allClear(){
   this->error = false;
   this->display = 0;
   this->oper = ' ';
   this->result = 0; 
   this->memory = 0;
}
