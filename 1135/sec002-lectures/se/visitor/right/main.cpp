#include "tree.h"
#include "binary.h"
#include "unary.h"
#include "counter.h"
#include "printer.h"
#include <iostream>
using namespace std;

int main(){
    Tree * tp = new Unary("foo", new Binary("bar", new Unary("baz", NULL), new Binary("bat", NULL, new Unary("cat", NULL))));
    Counter c;
    tp->accept(c);
    cout << c.count << endl;
    Printer p;
    tp->accept(p);
}
