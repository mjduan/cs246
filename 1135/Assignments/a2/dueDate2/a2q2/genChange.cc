#include <iostream>
using namespace std; // direct naming 

/*************************************************************************
/ CS 246 Assignment 2 Part 2
/
/ Quiestion 2 
/ genChange: Takes coin denominations and total value from Standard Input
/	     Calculates combination of coins to make total, if possible 
*************************************************************************/

int main(){
		
	int coinTypes, i; // i is a counter
	cin >> coinTypes; // read in total number of diff coins 
								//	cout << "coinTypes is: " << coinTypes << endl;
	
	// if number of input 
	if ( coinTypes > 10 ) {
		cout << "Impossible" << endl; 
		return 42;
	}
	int types[coinTypes]; // array for coin types
	

    // reads input into array
	for ( i = 0; i < coinTypes; i++ ){
    	if (cin.eof()){
        	cout << "Impossible" << endl;      
			return 42;
		}
		cin >> types[i]; //  read new coin type from cin
	}

	int total = 0;  // initiates at 0 
	cin >> total; // reads in total
								//	cout << "total is: " << total << endl; 
   
	int numCoin[coinTypes]; // array for storing number of each coin for change 
	
	for ( i = 0; i < coinTypes; i++ ){
	   numCoin[i] = 0; // initalizes numCoin with entries of 0 
	} 
								//	cout << "numCoin initialized" << endl;   
  
    int flag = 0; // flag to see if subsequent cond is entered
 
	
	if ( total != 0 ) {
 		flag = 1;
 		
		for ( i = 0; i < coinTypes; i++ ) {
	     if ( total < types[i] ) continue; 	
         	int div = total / types[i];
								//	cout << "div is: " << div << endl;
        	int mod = total % types[i];
								//	cout << "mod is: " << div << endl;
		//
		//	if (div == 0) {
		//		cout << "Impossible"; 
		//		return 42;
		//	i}        
         
	    	if ( mod == 0 ){ // if total divides evenly, break  
	    		numCoin[i] = div; 
				total = total - div * types[i];
				break;
	    	} else {
      			numCoin[i] = div;
								//	cout << "numCoin[i] is: " << numCoin[i] << endl;      		 
     			total = total - div * types[i];
								//	cout << "new total is: " << total << endl; 
    		}
		}
	}
   	if ( total != 0 ) {
		cout << "Impossible" << endl;
		return 42; 
	} 

   if ( flag == 0 ) return 42; // if above loop was not entered, return nothing      
    
    // loop for output
	for (i = 0; i < coinTypes; i += 1){
      if ( numCoin[i] == 0 ) continue; 	
 		cout << numCoin[i] << " x " << types[i] << endl;
    }
	
}






