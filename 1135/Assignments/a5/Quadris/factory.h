#ifndef __FACTORY_H__
#define __FACTORY_H__
#include <iostream>
//#include "block.h"

class Block; // forward class declaration

class Factory{
	
	public:
		Factory();
		//Factory& operator=(const Factory& rhs);
		~Factory();
		virtual Block* makeBlock()=0;
};

#endif
