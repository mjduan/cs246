#include<iostream>
using namespace std;

struct Base{
    int i;
    int r(int j) {cout << "Base::r" << endl; return j + i;}
    Base() : i(0) {cout << "Base()" << endl;}
   
};
struct Derived {
    Base b;
    int s(int k) { cout << "Derived::s" << endl;b.i = k * 2; b.r(k);}
    Derived(){cout << "Derived()" << endl;} 
    // Note: b's default ctor is implicitly called
};
int main(){
    Derived d;
    d.b.i = 3;
    d.b.r(7);
    d.s(6);
}
