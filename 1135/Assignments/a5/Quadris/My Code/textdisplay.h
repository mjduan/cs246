#ifndef __TEXTDISPLAY_H__
#define __TEXTDISPLAY_H__
#include <iostream>
#include <sstream>

const int displayRows = 15;
const int displayCols = 10;

class TextDisplay {
	char **theDisplay;
 public:
  TextDisplay();

  void notify(int r, int c, char ch);

  ~TextDisplay();

  friend std::ostream &operator<<(std::ostream &out, const TextDisplay &td);
};

std::ostream& operator<<(std::ostream &out, const TextDisplay &td);

#endif
