/***********************************************************************
*  CS 246 Assignment 4 Question 3
*
*
*  file: textdisplay.cc
*  Definitions for methods in Textdisplay class
************************************************************************/

#include <iostream>
#include "cell.h"
using namespace std; 

// parameter constructor, creates text display of size n 
TextDisplay::TextDisplay( int n ):gridSize(n){
   theDisplay = new char*[n];
	//cout << "text display constructor called" << endl; 	    
   for( int r = 0; r < n; ++r ){
      theDisplay[r] = new char[n]; 
      for ( int c = 0; c < n; ++c ) {
         theDisplay[r][c] = '_'; 
      }
   }
}

void TextDisplay::notify(int r, int c, char ch) {
	//cout << "!==td::notify() called" << endl; 
	//cout << "	--cell is: " << r << " x " << c << endl; 
	//cout << "	--char is: " << ch << endl;  
   theDisplay[r][c] = ch;
}

TextDisplay::~TextDisplay(){
  for( int r = 0; r < gridSize; ++r ){      
      delete [] theDisplay[r];
   }
   delete [] theDisplay; 
}

ostream &operator<<(ostream &out, const TextDisplay &td){
	int r = 0;
	// prints out up to the last row
   for (; r < td.gridSize-1; ++r){
		for (int c = 0; c < td.gridSize; ++c){
			out << td.theDisplay[r][c];
		}
		out << endl;
   } 
	// print out last row
	for (int c = 0; c < td.gridSize; ++c){
		out << td.theDisplay[r][c];
	}
	
	return out;
}
