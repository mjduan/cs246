/***********************************************************************
*  CS 246 Assignment 4 Question 3
*
*
*  file: textdisplay.cc
*  Definitions for methods in Textdisplay class
************************************************************************/

#include <iostream>
#include "cell.h"
#include "textdisplay.h"
using namespace std; 

// parameter constructor, creates text display of size n 
TextDisplay::TextDisplay(){
   theDisplay = new char*[displayRows];
	//cout << "text display constructor called" << endl; 	    
   for( int r = 0; r < displayRows; ++r ){
      theDisplay[r] = new char[displayCols]; 
      for ( int c = 0; c < displayCols; ++c ) {
         theDisplay[r][c] = '_'; 
      }
   }
}

void TextDisplay::notify(int r, int c, char ch) {
	//cout << "!==td::notify() called" << endl; 
	//cout << "	--cell is: " << r << " x " << c << endl; 
	//cout << "	--char is: " << ch << endl;
	if ((r - 3) < 0 || c < 0 || (r-3) >= displayRows || c >= displayCols ) {return;}  
   theDisplay[r - 3][c] = ch;
}

TextDisplay::~TextDisplay(){
  for( int r = 0; r < displayRows; ++r ){      
      delete [] theDisplay[r];
   }
   delete [] theDisplay; 
}

ostream &operator<<(ostream &out, const TextDisplay &td){
	int r = 0;
	// prints out up to the last row
	cout << "Level:       " << 1 << endl; 
	cout << "Score:       " << "score" << endl;
	cout << "Hi Score: "    << "high score" << endl; 
	cout << "---------------" << endl; 

   for (; r < displayRows-1; ++r){
		for (int c = 0; c < displayCols; ++c){
			out << td.theDisplay[r][c];
		}
		out << endl;
   } 
	// print out last row
	for (int c = 0; c < displayRows; ++c){
		out << td.theDisplay[r][c];
	}
	cout << endl;
	cout << "---------------" << endl; 
	cout << "Next:" << endl;
	cout << "put block here, overload operator<< to print block" << endl; 	
	return out;
}
