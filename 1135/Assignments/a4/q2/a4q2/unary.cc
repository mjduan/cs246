/*********************************************************************
*  CS 246 Assignment 4 Question 2
*
*
*  file: unary.cc
*  Definitions for methods in Unary class, derived class of Expression 
**********************************************************************/

#include <iostream>
#include <string>
#include "unary.h"
#include "binary.h"
using namespace std;

// default constructor
Unary::Unary():
   Expression(),
   expr(NULL),
   op("") {} 

// parameter constructor
Unary::Unary( Expression* target, string op ): Expression(*target), expr(target), op(op) { 
   evaluate();
}  

// copy constructor
Unary::Unary( const Unary& target ):
   Expression( target ), // implicit slicing
   expr( target.expr ),
   op( target.op ) {} 

// member operator= 
Unary& Unary::operator=(const Unary &rhs){
   if (this == &rhs) return *this; 
   Expression::operator=(rhs); 
   expr = rhs.expr; 
   op = rhs.op; 
   return *this;
}

// Pretty print: Prints infix notation along with value of expression
void Unary::prettyprint(){
   cout << getInfix() << endl; 
	cout << "= " << getValue() << endl;
}

// evaluates the Unary object based on (op) and (expr)
void Unary::evaluate(){

   if ( op == "ABS" ){
      if ( getValue() < 0 ) {
         setValue( -1 * getValue() ); 
      }
      setInfix( "|" + getInfix() + "|");
   }
  
   // checks if Expr is Binary using dynamic_cast
   // returns null ptr if object referred doesn't contain type casted  
     
	else if (op == "NEG") {
		setValue( -1 * getValue() );      
      
		//Binary* bp;
		//bp = dynamic_cast<Binary*>(expr);

		//if (bp == 0){ // expr is not a bin 
		setInfix( "-" + getInfix() );             
		//} else {
		//	setInfix( "-(" + getInfix() + ")");      
	}
}

// getter definition
Expression* Unary::getExpr(){ return expr; }

string Unary::getOp(){ return op; }

