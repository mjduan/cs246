#include <iostream>

void errFunc(int code){
    switch  (code){
	case 1 :
	    std::cerr << "Bad filename" << std::endl;
	    break;
        case 2:
	    std::cerr << "Division by zero" << std::endl;
	    break;
        default:
	    std::cerr << "Danger Will Robinson! Danger!" << std::endl;
    }
}
