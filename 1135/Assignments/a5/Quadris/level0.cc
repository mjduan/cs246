#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "level0.h"
using namespace std; 

// default constructor
Level0::Level0(){
	ifstream file("sequence.txt");
	sequence << file.rdbuf();   
	file.close(); 
}

// default destructor
Level0::~Level0(){
  
}

Block* Level0::makeBlock(){
	char c; 
	sequence >> c; 
	Block* rtn; 
	rtn = NULL;
	if ( sequence.fail() ){return NULL;}
	if ( c == 'Z' ){
		cout << "new Z-Block" << endl;
		//rtn = new zBlock;
	} else if ( c == 'S' ){
		cout << "new S-Block" << endl;
		//rtn new sBlock;
	} else if ( c == 'L' ){
     	cout << "new L-Block" << endl;
		//rtn = new lBlock; 
	} else if ( c == 'J'){
		cout << "new J-Block" << endl;
		//rtn = new jBlock; 
	} else if ( c == 'T'){
		cout << "new T-Block" << endl;
		//rtn = new tBlock; 
	} else if ( c == 'O' ){
		cout << "new O-Block" << endl;
		//rtn = new oBlock; 
	} else if ( c == 'I' ){
		cout << "new I-Block" << endl;
		//rtn = new iBlock;
	}
	return rtn;
}
