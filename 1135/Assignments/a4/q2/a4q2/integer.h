#ifndef __INTEGER_H__
#define __INTEGER_H__

#include <iostream>
#include <string>
#include "expression.h"

class Integer: public Expression{
   
   public: 
      Integer(); // default constructor 
      Integer( const long int i ); // param constructor
      Integer( const Integer& ); // copy constructor
      Integer& operator=(const Integer& rhs);

   
      void prettyprint();
      void evaluate();
      
      
};

std::string intToString (int num);      
#endif 
