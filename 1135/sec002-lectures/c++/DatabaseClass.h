#include <string>

class Database{
    unsigned int users;
    static void clean();
    Database();
    // Prevent copy and destructor calls
    ~Database(){}
    Database(const Database&){}
    Database& operator=(const Database&){}
    // maybe some internal database structures
    // ...
  public:
    static Database* singleton;
    static Database* getInstance();
    void addUser(std::string id);
    unsigned int getCount() const;  
};
