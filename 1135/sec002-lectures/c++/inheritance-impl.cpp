#include<iostream>
using namespace std;


struct Base{
    int i;
    int r(int j) {cout << "Base::r" << endl; return j + i;}
    Base() : i(0) {cout << "Base()" << endl;}
   
};
struct Derived : public Base{
    int s(int k) { cout << "Derived::s" << endl; i = k * 2; r(k);}
    Derived(){cout << "Derived()" << endl;}
};

int main(){
    Derived d;
    d.i = 3;
    d.r(7);
    d.s(6);
}
