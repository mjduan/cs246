#include <iostream>
#include <string>
#include <sstream>
#include "textprocess.h"
#include "swapcase.h"
using namespace std;

void SwapCase::setSource(std::istream *inp){
	source = inp;
}

string SwapCase::getWord(){
	//cout << "[SwapCase::getWord called]" << endl;	
	string s;
	s = component->getWord();
	//cout << "s is now: " << s  << endl;
	//cout << "s before:" << s << endl;
	for ( unsigned int i = 0; i < s.size() ; ++i ) {
		//cout << "!--s is: " << s << endl; 
		//cout << "!--j is: " << j << endl;
		if (s[i] <= 'Z'  && s[i] >= 'A'){
			s[i] += 32;	
		} else if (s[i] <= 'z' && s[i] >= 'a'){
			s[i] -= 32;
		}
	}
	//cout << "s after: " << s << endl; 
	failed = source->fail(); 
	return s; 
}

bool SwapCase::fail() const {
	return failed; 
}

// Parameter constructor 
SwapCase::SwapCase(TextProcessor *tp, istream* in){
	component = tp;
	source = in; 
	component->setSource(in); 
}

// destructor
SwapCase::~SwapCase(){
	delete component;
}

