#ifndef __LETTUCE_H__
#define __LETTUCE_H__
#include "tacodecorator.h"


class Lettuce : public TacoDecorator{
  public:
    Lettuce(Taco& t);
    double cost(); 
    void desc();
};
#endif
