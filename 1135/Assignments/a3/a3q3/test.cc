#include <iostream>
#include <fstream>
#include <locale>
#include <string>
#include "test.h"
using namespace std; 

// ancestor: adds name as ancestor to tree
FTree* ancestor( FTree& root, const string ancestor ){
   //cout << "!---- ANCESTOR called" << endl; 
   FTree* retTree = new FTree; 
   char c; 
   //   cout << "ancestor start: enter prompt: "; cin >> c;
 
   retTree->name = ancestor; 
     // cout << "!--- ancestor is now: " << retTree->name << endl;
   retTree->children[0] = &root; 
      //cout << "!--- child[0] is now: " << retTree->children[0] << endl;
   retTree->numChildren += 1;    
      //cout << "!--- numChildren is now: " << retTree->numChildren << endl;
      
     // cout << "ancestor end: enter prompt: "; cin >> c; 
   
   return retTree;
}

// addNode: FTree + string + string --> FTree 
bool addNode( FTree& tree, const string parent, const string child ){
   //cout << "!---- ADDNODE called" << endl;    
   
   if (&tree == 0){ return false; } // base false case 
   
   if ( tree.name == parent ) { 
      if ( tree.numChildren >= 10 ){ return false; }

      FTree* newChild = new FTree;      
      newChild->name = child; 
      tree.children[tree.numChildren] = newChild; 
      tree.numChildren++; 
      return true;
   }
   for ( int i = 0; i < tree.numChildren; i++ ){
      //cout << i << "th recursive call" << endl; 
      if ( addNode(*tree.children[i], parent, child) ) { 
         
         return true; 
      }
   }
//   char c; cout << "addNode end: enter prompt: "; cin >> c; 
   return false; 
}

void print( const FTree& tree ){
   //cout << "!---- PRINT called" << endl;   
   int numC = tree.numChildren; 

   if (numC > 0) {
      cout << tree.name << " is parent of "; 
      
      for (int i = 0; i < numC-1; i++){
         cout << tree.children[i]->name << ","; 
      }
      
      cout << (*tree.children[numC-1]).name << endl;
         
      for (int i = 0; i < numC; i++){    
         print(*(tree.children[i])); 
      }    
   }
}

// default constructor
FTree::FTree(){
   //cout << "!---- default constructor called" << endl; 
   this->name = " ";
   this->numChildren = 0; 
   for (int i = 0; i < maxChildren; i++){
      this->children[i] = NULL;
   }
   //char c; cout << "enter prompt: "; cin >> c; 
}

// parameter constructor
FTree::FTree(const string str){
   //cout << "!---- param constructor called" << endl; 
   this->name = str;
   this->numChildren = 0; 
   for (int i = 0; i < maxChildren; i++){
      this->children[i] = NULL;
   }
   //char c; cout << "constructor end: enter prompt: "; cin >> c; 
}

// destructor 
FTree::~FTree(){
   //cout << "!---- DESTRUCTOR called" << endl; 
   //cout << "      this object is: " << this->name << endl; 
   if (this->numChildren > 0){
      for (int i = 0; i < this->numChildren; i++){
         delete this->children[i];      
      }
   }
   //char c; cout << "destructor end: enter prompt: "; cin >> c; 
}

// copy constructor
FTree::FTree(const FTree& tree){
   //cout << "!---- copy constructor called" << endl; 
   this->name = tree.name; 
   this->numChildren = tree.numChildren; 

   for (int i = 0; i < tree.numChildren; i++){
      this->children[i] = tree.children[i];
   }
   //char c; cout << "copy con end: enter prompt: "; cin >> c; 
}

//swap method, used by copy constructor
void FTree::swap(FTree& rhs){
   //cout << "!---- SWAP called" << endl; 
   string tempName = rhs.name; 
   int tempNum = rhs.numChildren; 
   FTree* tempChild[maxChildren];
   
   // copy rhs into temp 
   for (int i = 0; i < rhs.numChildren; i++) {
      tempChild[i] = rhs.children[i];
   }
   
   rhs.name = this->name; 
   this->name = tempName;
   rhs.numChildren = this->numChildren; 
   this->numChildren = tempNum; 

   // copy this->children into rhs
   for (int i = 0; i < maxChildren; i++) {
      rhs.children[i] = this->children[i];
   }
   // rhs.children = this->children; 

   // copy temp into this
   for (int i = 0; i < maxChildren; i++) {
      this->children[i] = tempChild[i];
   }
   //this->children = tempChild;      
 
   //cout << "swap end:" << endl;
     
}

//operator=
FTree& FTree::operator=(const FTree& rhs){
   //cout << "!---- OPERATOR= called" << endl;
   FTree temp = rhs;     
	swap(temp); 

   //cout << "operator= end" << endl;    
   return *this;
}

// nodePath: FTree&, String --> String 
//    Returns a path from the root to the given name as a string
//    if node is not in FTree, return " "
string nodePath( FTree& node, const string name, string ret){
   //cout << "!---- NODEPATH called" << endl;  
   
   if ( node.name == " " ){ return ret; }
   
   //                                          cout << "1st if end" << endl; 
   if ( &node == 0 ) { 
   //   cout << "base case, & == 0" << endl; 
      return ret; 
   }  
   //                                          cout << "2nd if end" << endl; 
   if ( node.name == name ) {
      ret = node.name; 
   //   cout << ">>>>>>>> node found, node name: " << node.name << "ret is now: " << ret << endl;
      return ret; 
   } 
   //                                           cout << "3rd if end" << endl; 
   for ( int i = 0; i < node.numChildren; i++ ){
   //   cout << "recursive call[" << i << "]" << endl;
      
      ret = nodePath(*node.children[i], name, ret);
      
      if ( ( ret != " ") ){
          //cout << ">>>>>> ret is now: " << ret << endl; 
          ret = (node.name + "," + ret);
          return ret; 
      }
   } 
   //                                             cout << "4th for end" << endl; 
   // char c; cout << "nodePath end: enter prompt: "; cin >> c; 
   return " ";
}

// FTree, String --> Void
//    Deletes String and it's children from FTree
//    if String is not present, it does nothing

void delNode(FTree& tree, const string target){
   //cout << ">>>>>>>>>>delNode called" << endl; 
   // case for root.name = target 
   if ( tree.name == target ) {
      //cout << ">>>>>>>>>> target is root" << endl; 
      // delete all children 
      for (int i = 0; i < tree.numChildren; i++){
        delete tree.children[i]; 
      }
      // reset root to base root
      tree.name = " ";
      tree.numChildren = 0;     
      for (int i = 0; i < maxChildren; i++){
         tree.children[i] = NULL;
      }
   } 
   // check if children are target
   for (int i = 0; i < tree.numChildren; i++){
      if (tree.children[i]->name == target){
         //cout << ">>>>>>>>>> Target found" << endl; 
         
         delete tree.children[i];
         tree.numChildren -= 1; 
         
         // shift over array 
         for ( ; i < maxChildren - 2; i++ ){
         //cout << ">>>>>>>>>> array shifting[" << i << "]"<< endl;
            tree.children[i] = tree.children[i+1];         
         }
         tree.children[i+2] = NULL; 

         return;
      }
   }        
   // recursive call
   for (int i = 0; i < tree.numChildren; i++){
      delNode(*(tree.children[i]), target);
   }
   //cout << ">>>>>>>>>>delNode called" << endl; 
}

/*
void delAll (FTree& tree){
   //cout << ">>>>>>delAll called" << endl;
   
   //for ( int i = 0; i < tree.numChildren; i++ ){
     // delAll(*(tree.children[i]));
   delete &tree;
   
   cout << ">>>>>>delAll end" << endl; 
   return; 
   //}
}
*/



/*
// findNode: FTree, string --> FTree
FTree* findNode(FTree& tree, const string name){
   cout << "!---- FINDNODE called" << endl; 
   if (&tree == 0){ return NULL;} // base case, if not in tree
   
   if ( tree.name == name) return &tree; 
   
   for ( int i = 0; i < tree.numChildren; i++ ){
      FTree* temp = findNode(*tree.children[i], name);
      if ( temp != NULL ) 
         return temp;
   } 
   char c; 
      cout << "findNode end: enter prompt: ";
      cin >> c; 
   return NULL;
}

// FTree --> void
// given a node, deletes it and it's subsequent children
void delTree( FTree& tree ) {
   cout << "!---- DELTREE called" << endl; 
   for ( int i = 0; i < tree.numChildren; i++ ){
      if ( !tree.children[i] == 0 ){
         delTree(*tree.children[i]);
      }
      delete &tree; 
   }
   char c; cout << "delTree end: enter prompt: "; cin >> c; 
}
*/



