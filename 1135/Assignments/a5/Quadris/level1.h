#ifndef __LEVEL1_H__
#define __LEVEL1_H__

#include <iostream>
#include "factory.h"

//class Block;

class Level1: public Factory{

	public: 
		Block* makeBlock();	

};
#endif
