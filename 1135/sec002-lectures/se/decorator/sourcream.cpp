#include "sourcream.h"
#include <iostream>
using namespace std;

SourCream::SourCream(Taco& t) : TacoDecorator(t){}
double SourCream::cost() { return t.cost() + 0.75;}
void SourCream::desc() { t.desc(); cout << ", sour cream";}
