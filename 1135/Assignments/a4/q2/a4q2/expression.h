#ifndef __EXPRESSION_H__
#define __EXPRESSION_H__

#include <iostream>
#include <string>

class Expression{
	std::string infix; // string representing infix notation
	long int value;

	public: 
		Expression(); // constructor
		Expression( const std::string expr, long int i); // param constructor
		Expression( const Expression &expr );       // copy constructor 
		Expression& operator=(const Expression& rhs); // operator= 
		virtual ~Expression(); // pure virtrual destructor
      
		virtual void prettyprint();      
		virtual void evaluate();
      
		// getters
		std::string getInfix();
		long int getValue();
		// setters 
		void setInfix( std::string str);   
		void setValue( long int i ); 
		
};

#endif 
