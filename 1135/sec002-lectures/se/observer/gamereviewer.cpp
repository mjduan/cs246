#include "gamereviewer.h"
using namespace std;

GameReviewer::GameReviewer(string site) : site(site){}
void GameReviewer::review(string game){
    latest.name = game + " Best. Review. Ever.";
    latest.url = site + "/" + game;
    latest.summary = "Catch " + game + " Review Fever!";
    notifySubs();
};

void GameReviewer::notifySubs(){
    for(int i=0; i < maxSubs; ++i){
        if(subs[i] != NULL) {
            subs[i]->notify(latest);
        }
    }
}
