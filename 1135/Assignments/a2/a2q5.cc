#include <iostream>
using namespace std;

const int capacity = 25;

struct IntArray {
  int size;
  int contents[capacity];
};

IntArray readIntArray();

void printIntArray(IntArray* ia);


// Do not change this function!

int main() {  // Test harness for IntArray functions.
  bool done = false;
  IntArray a[4];
  while(!done) {
    char c;
    char which;
    cerr << "Command?" << endl;  // Valid commands:  ra, rb, rc, rd
                                 //                  pa, pb, pc, pd, q
    cin >> c;  // Reads r, p, or q
    if (cin.eof()) break;
    switch(c) {
      case 'r':
        cin >> which;  // Reads a, b, c, or d
        a[which-'a'] = readIntArray();
        break;
      case 'p':
        cin >> which;  // Reads a, b, c, or d
        printIntArray(&a[which-'a']);
        break;
      case 'q':
        done = true;
    }
  }
}
