#ifndef __BEEF_H__
#define __BEEF_H__
#include "tacodecorator.h"


class Beef : public TacoDecorator{
  public:
    Beef(Taco& t);
    double cost(); 
    void desc();
};
#endif
