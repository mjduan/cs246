#include "Database.h"

using namespace std;

Database * Database::singleton = NULL;
Database::Database() : users(0){}
Database* Database::getInstance(){
    if(singleton) return singleton;
    singleton = new Database;
    return singleton;
}

void Database::addUser(string id){ users += 1;}

unsigned int Database::getCount(){ return users;}

