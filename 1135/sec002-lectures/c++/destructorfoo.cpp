#include <iostream>
using namespace std;

struct Foo {
    int * arr;
    int i;
    // Other stuff
    Foo(int j) : i(j), arr(new int[10]){}
    Foo(const Foo& f) : i(f.i + 1), arr(new int[10]) {}
    // Other constructors ...
    ~Foo(){ delete [] arr; cerr << "I was " << i << endl;}
};

int main(){
    Foo f1(1);
    Foo f2(f1);
    Foo * fp = new Foo(3);
    delete fp;

}
