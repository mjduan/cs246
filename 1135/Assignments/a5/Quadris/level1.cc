#include <iostream>
#include <cstdlib>
#include "level1.h"
using namespace std;

Block* Level1::makeBlock(){
	int num = rand() % 12;
	Block* rtn;
	rtn = NULL;
	switch(num){ 
		case (0):{
			//rtn = new zBlock;		
			cout << "!==new Z-Block" << endl;
			break;
		}
		case (1):{
			//rtn = new sBlock;
			cout << "!==new S-Block" << endl;
			break; 
		}
		case (2): case(3):{
			//rtn = new lBlock; 
			cout << "!==new L-Block" << endl;
			break; 
		}
		case (4): case(5):{
			//rtn  = new jBlock; 
			cout << "!==new J-Block" << endl;
			break;
		}
		case (6): case(7):{
			//rtn = new tBlock; 	
			cout << "!==new T-Block" << endl;
			break; 
		}
		case (8): case(9):{
			//rtn = new oBlock;
			cout << "!==new O-Block" << endl;
			break;
		}
		case (10): case(11):{
			//rtn = new iBlock;
			cout << "!==new I-Block" << endl;
			break;
		}
	}
	return rtn;
}
