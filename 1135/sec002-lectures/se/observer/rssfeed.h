#ifndef __RSSFEED_H__
#define __RSSFEED_H__
#include "webobserver.h"
#include "article.h"
#include "website.h"

const int maxWebs = 10;
struct RSSFeed : public WebObserver {
  Website * websites[maxWebs];
  RSSFeed();
  void sub(Website * w);
  void unsub(Website * w);
  void notify(Article a);
  ~RSSFeed();
};

#endif
