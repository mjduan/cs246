#include "DatabaseFixed.h"
#include <iostream>
using namespace std;

int main(){
    Database &db1 = *Database::getInstance();
    Database *db2 = Database::getInstance();
    db1.addUser("Morpheus");
    db2->addUser("Trinity");
    Database::getInstance()->addUser("Neo");
    cout << "db1 user count: " << db1.getCount() << endl;
    cout << "db2 user count: " << db2->getCount() << endl;
    cout << "dynamic user count: " << Database::getInstance()->getCount() << endl;
}
