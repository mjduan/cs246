#include "binary.h"
#include "visitor.h"

Binary::Binary(std::string data, Tree * left, Tree * right) : Tree(data), left(left), right(right) {}

void Binary::accept(Visitor &v) {
    // In order traversal
    if(left != NULL) left->accept(v);
    v.visit(*this);
    if(right != NULL) right->accept(v);
}

Binary::~Binary(){ if(left != NULL) delete left; if(right != NULL) delete right;}
