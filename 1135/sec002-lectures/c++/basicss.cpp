#include <iostream>
#include <string>
#include <sstream>
using namespace std;

int main(){
  string line;
  stringstream ssout;
  int x,y,z;
  while(getline(cin, line)){
    stringstream ssin;
    // Give input to ssin, could also be char*
    ssin.str(line);
    // Could also have done ssin << line;
    ssin >> x >> y >> z;
    int sum = x + y + z;
    // Note that endl just appends a new line
    // No Buffer to flush
    ssout << "Line sum is: " << sum << endl;
  }
  // Print output
  cout << ssout.str();
  // or
  // string s = ssout.str();
  // cout << s;
}

