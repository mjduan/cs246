#include <iostream>
using namespace std;

struct Base{
    Base() { cout << "Base()" << endl;}  
    Base(const Base& o){ cout << "Base(Base&)" << endl;}
    Base& operator=(const Base& b){cout <<"Base =" << endl;}
};
struct Derived : public Base{
  int i; // basic type => bitwise copy  

};
int main(){
    Derived a;
    Derived b = a;
    b = a;
}
