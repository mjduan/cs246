/************************************************************************
* CS 246 Assignment 2
*
*
* Question 5 part b)
*
* Array Structure
*************************************************************************/


#include <iostream>
#include <sstream>
using namespace std;

const int capacity = 25;

struct IntArray {
  int size;
  int contents[capacity];
};

istream& operator>> (istream& in, stringstream& ss){// overloads >> operator 
    string s;
    in >> s;
    ss << s << endl;
    return in;
} 

// PRE: Void 
// POST: IntArr
//	 Reads as many ints as will fit in IntArray (dictated by capacity) from stdin
// 	 Will stop reading after first non-int token is encountered. The token is then
//   stored inside the stream, where it is accessible by other programs.

IntArray readIntArray(){

	stringstream iss; // input string stream  
	int i; // i for index

	IntArray retArr; // declare new array struct
	retArr.size = 0; // initalize size field

	for ( i = 0; i < capacity; i++ ){ // read from stdin into input stringstream
		cin >> iss;  
	}	

	string finalString = iss.str(); 
	cout << "!!----the final string is: " << finalString  << endl; 

	int temp; 
	for ( i = 0; i < capacity; i++) { // loop for filling array 
		iss >> temp; 
	  if ( iss.fail() ) break; 
		retArr.contents[i] = temp; 
		retArr.size += 1; 	
	
		cout << "!!----arr[" << i <<"] is: "  << retArr.contents[i] << endl;
		cout << "!!----size is: " << retArr.size << endl; 
	}
	return retArr;
} 

void printIntArray(IntArray* ia){
	int i;
	for ( i = 0; i < (*ia).size; i++ ){
		cout << (*ia).contents[i] << " ";	
	}	
	cout << endl;
}


// Do not change this function!

int main() {  // Test harness for IntArray functions.
  bool done = false;
  IntArray a[4];
  while(!done) {
    char c;
    char which;
    cerr << "Command?" << endl;  // Valid commands:  ra, rb, rc, rd
                                 //                  pa, pb, pc, pd, q
    cin >> c;  // Reads r, p, or q
    if (cin.eof()) break;
    switch(c) {
      case 'r':
        cin >> which;  // Reads a, b, c, or d
        a[which-'a'] = readIntArray();
        break;
      case 'p':
        cin >> which;  // Reads a, b, c, or d
        printIntArray(&a[which-'a']);
        break;
      case 'q':
        done = true;
    }
  }
}
