/************************************************************
/ CS 246: Assignment 2 
/
/ Question 4 b): String parsing 
*************************************************************/

#include <iostream>
#include <string>
using namespace std;

int main (){
	int words; 
	string line;

	while ( !cin.eof() ){	// loop while not eof
		
		cin >> words; // read in number of words
//		cout << "words: " << words << endl;	
 
	  if ( cin.fail() || !getline(cin,line) ){
		cin.clear();
		cin.ignore();
		continue; 	  
	  }
//		cout << "line: " << line << endl;
//		cout << "line[0] is; " << line[0] << endl;
	    		
//		cout << "line[1] is; " << line[1] << endl;
//	    	cout << "line[2] is; " << line[2] << endl;
//		cout << "len of line is: " << line.length() << endl;	
		int i; // counter for indexing the string 
		int flag = 0; // flag == 0 --> white space last encountered
					  // flag == 1 --> char last encountered

		int count = 0; // countis number of words

		for ( i = 0; i < line.length(); i++ ){

//			cout << line[i] << endl;
			// if non-wspace  was last and encounter wspace, flag = 0
			if ( flag == 1 && (line[i] == ' ' || line[i] == '\t' )) {
				flag = 0;
			// if wspace last, & non-wspace encountered, count++, flag = 1
			} else if (flag == 0 && !(line[i] == ' ' || line[i] == '\t' )) {
				flag = 1; 
				count++;
			} else continue;
			
//		cout << "count: " << count << endl ; 	
		}

  if ( count == words ) continue;
	
	// else ouput count and line
	cout << count << line << endl;  
	}
 	
	return 0;

