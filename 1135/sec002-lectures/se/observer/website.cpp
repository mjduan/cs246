#include "website.h"

Website::Website(){
    for(int i=0; i < maxSubs; ++i) subs[i] = NULL;
};

void Website::attach(WebObserver &s){
   for(int i=0; i < maxSubs; ++i){
        if(subs[i] == NULL) {
            subs[i] = &s;
            break;
        }
    }
}

void Website::detach(WebObserver &s){
    for(int i=0; i < maxSubs; ++i){
        if(subs[i] == &s) {
            subs[i] = NULL;
            break;
        }
    }
}
