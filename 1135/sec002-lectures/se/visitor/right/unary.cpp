#include "unary.h"
#include "visitor.h"
Unary::Unary(std::string data, Tree * child) : Tree(data), child(child) {}

void Unary::accept(Visitor &v) {
    if(child != NULL) child->accept(v);
    v.visit(*this);
}

Unary::~Unary(){ if(child != NULL) delete child;}
