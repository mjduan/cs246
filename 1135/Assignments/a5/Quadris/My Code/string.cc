#include <iostream>
#include <string>
#include <algorithm> 
#include <sstream>
//#include <cstring>
using namespace std;

int main () {

	//commands
      string left = "left";
      string right = "right";
      string down = "down";
      string drop = "drop";
      string cw = "clockwise";
      string ccw = "counterclockwise";
      string lvUp = "levelup";
      string lvDown = "leveldown";
      string restart = "restart";

	
	while (!cin.eof()){ 

		string command, comm; 
		stringstream buffer;
		int mult = 1; 
		
		getline(cin, command); 
		buffer << command; 
		// attempt to read multiplyer
		buffer >> mult; 
		if (buffer.fail()){
			cout << "no mult " << mult << endl; 
			buffer.clear();
			mult = 1; // reset mult to 1 
		}

		// read first white space deliminated string from command

		buffer >> comm; 
		cout << "	[comm is: " << comm << " [length is: " << comm.size()  << endl;
		if (comm.size() <= 1) continue; 
		
		// check if command is left
		if ( comm.size() <= left.size() ){
			if ( ( left.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 3 ){
				for ( int i = 0; i < mult; ++i){
					cout << "!==left detected, executing function calls" << endl;
				}
			} 
		}
		if ( comm.size() <= right.size() ){
			if ( ( right.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            for (int i = 0; i < mult; ++i) {
					cout << "!==right detected, executing function calls" << endl;
				}
         }
      } 
		if ( comm.size() <= down.size() ){
			if ( ( down.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            for (int i = 0; i < mult; ++i){
					cout << "!==down detected, executing function calls" << endl;
				}
         }
      } 
	   if ( comm.size() <= drop.size() ){
         if ( ( drop.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            for (int i = 0; i < mult; ++i){
					cout << "!==drop detected, executing function calls" << endl;
				}
         }
      } 
		if ( comm.size() <= cw.size() ){
         if ( ( cw.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            for (int i = 0; i < mult; ++i){
					cout << "!==cw detected, executing function calls" << endl;
				}
         }
      } 
		if ( comm.size() <= ccw.size() ){
         if ( ( ccw.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            for (int i = 0; i < mult; ++i){
					cout << "!==ccw detected, executing function calls" << endl;
				}
         }
      }
		if ( comm.size() <= lvUp.size() ){
         if ( ( lvUp.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 6 ){
            for (int i = 0; i < mult; ++i){
					cout << "!==lvUp detected, executing function calls" << endl;
				}
         }
      }
		if ( comm.size() <= lvDown.size() ){
         if ( ( lvDown.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 6 ){
				for (int i = 0; i < mult; ++i){
          	  cout << "!==lvDown detected, executing function calls" << endl;
				}
         }
      } 
	   if ( comm.size() <= restart.size() ){
         if ( ( restart.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
         	cout << "!==restart detected, executing function calls" << endl;
         }
      }
	}
/*
	string s1 = "l"; 
	string s2 = "le";
	string s3 = "lef"; 
	string s4 = "left";
	string s5 = "leftist";
	cout << "s1: " << left.compare(0,1,s1) << endl; 
	cout << "s2: " << left.compare(0,2,s2) << endl;
	cout << "s3: " << left.compare(0,3,s3) << endl;
	cout << "s4: " << left.compare(0,4,s4) << endl;
	cout << "s5: " << left.compare(s5) << endl;
*/	
	string t1 = "r"; 
	string t2 = "ri";
	string t3 = "rig"; 
	string t4 = "righ";
	string t5 = "right";
	
	cout << "t1: " << right.compare(0,1,t1) << endl; 
   cout << "t2: " << right.compare(0,2,t2) << endl;
   cout << "t3: " << right.compare(0,3,t3) << endl;
   cout << "t4: " << right.compare(0,4,t4) << endl;
   cout << "t5: " << right.compare(t5) << endl;



	return 0; 
}
