#include <iostream>
using namespace std;

struct Bar{
    void f(){ cout << "Bar::f" << endl;}
    virtual void h(){cout << "Bar::h" << endl;}
};

struct Foo : public Bar{
    void f(){cout << "Foo::f" << endl;}
    void g(){cout << "Foo::g" << endl;}
    void h(){cout << "Foo::h" << endl;}

};

int main(){
    Bar & bp = *new Foo;
    bp.f(); 
    ((Foo &) bp).f();
    bp.h();
    bp.Bar::h();
    // error to access g through bp directly, need to cast
    ((Foo &) bp).g();
    delete &bp;
}
