/*****************************************************************
* Assignment 4 Question 4
*
*  File: decorator.cc
*  Implementation of abstract virtual decorator class methods
*****************************************************************/

#include <iostream>
#include "decorator.h"
#include "textprocess.h"
using namespace std;

// default constructor
Decorator::Decorator():component(0){}

// parameter constructor
Decorator::Decorator(TextProcessor *component): component(component){}

Decorator::~Decorator(){}

