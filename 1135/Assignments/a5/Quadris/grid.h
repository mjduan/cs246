ifndef __GRID_H__
#define __GRID_H__
#include <iostream>
#include "cell.h"
#include "textdisplay.h"

const int rows = 18;
const int cols = 10; 

class Grid {
  Cell **theGrid;  // The actual grid.
  //int gridSize;    // Size of the grid.   
  void clearGrid();   // Frees the grid. 
  TextDisplay *td; // The text display.
  // Add private members, if necessary.
  void calcNeighbour (Cell* cell, const int &r, const int &c, const int &size);

 public:
  Grid();
  ~Grid();

  void newGrid(); // Sets up an n x n grid.  Clears old grid, if necessary.
  void tick();      // Runs one iteration of the simulation.
  void setFill(int r, int c, char fill);  // Sets cell at row r, col c to living.

  friend std::ostream& operator<<(std::ostream &out, const Grid &g);
};

std::ostream& operator<<(std::ostream &out, const Grid &g);

#endif
