#include <iostream>
using namespace std;

int main(){
  int n;
  for (;;){
    cout << "Enter a number: ";
    cin >> n;
    if (cin.fail()) {
      if (cin.eof()) break;
      cout << "Invalid number. Try again." << endl;
      cin.clear(); // reset stream failure
      // Clear next character 
      cin.ignore();
    } else {
      cout << "You entered: " << n << endl;
    }
  }
}
