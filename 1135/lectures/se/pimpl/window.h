#ifndef __WINDOW_H__
#define __WINDOW_H__
#include <iostream>
#include <string>

class XWindowImpl;

class Xwindow {
  XWindowImpl *pImpl;

 public:
  Xwindow(int width=500, int height=500);  // Constructor; displays the window.
  ~Xwindow();                              // Destructor; destroys the window.

  enum {White=0, Black, Red, Green, Blue}; // Available colours.

  // Draws a rectangle
  void fillRectangle(int x, int y, int width, int height, int colour=Black);

  // Draws a string
  void drawString(int x, int y, std::string msg);

};

#endif
