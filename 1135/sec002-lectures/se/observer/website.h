#ifndef __WEBSITE_H__
#define __WEBSITE_H__
#include "webobserver.h"
const int maxSubs = 10;

struct Website{
    WebObserver* subs[maxSubs];
    Website();
    virtual void notifySubs() = 0;
    void detach(WebObserver& s);
    void attach(WebObserver& s);
    virtual ~Website(){}
};
#endif
