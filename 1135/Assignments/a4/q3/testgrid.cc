#include <iostream>
#include <string> 
using namespace std; 

void calcNeigh (int r, int c, int size){
   int x, y; 
   for (int i = -1; i <= 1; i++) {
      for ( int j = -1; j <= 1; j++ ) {
         x = r + i; 
         y = c + j; 
         // if coordinates is self 
         if (x == r && y == c) { continue; }
         // if coordinates is out of bounds 
         if ( x < 0 || y < 0 || x >= size || y >= size ){ continue; }
         else {
           cout << "!----legit coords are: " << x << y << endl;           
         }
      }
   }
}


int main () {
   // initializing
   int n = 5; 
   
   int** g = new int*[n]; 
   
   for (int r = 0; r < n; r+=1){
      g[r] = new int[n]; 
      for (int c = 0; c < n; c+=1){
         g[r][c] = r*10 + + c; 
      }
   }
   
   // printing
   for (int i = 0; i < n; i+=1){
      for (int j = 0; j < n; j+=1){      
         cout << g[i][j] << ",";
      }
      cout << endl; 
   }
   

   calcNeigh(4, 1, 5); 
       
   
   //deleting
   for (int i = 0; i < n; i+=1){
      delete [] g[i]; 
   }
   delete [] g; 
   
   return 0;
}
