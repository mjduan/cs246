#ifndef __EMAILFEED_H__
#define __EMAILFEED_H__
#include "webobserver.h"

struct EmailFeed : public WebObserver {
  void notify(Article a);
};
#endif
