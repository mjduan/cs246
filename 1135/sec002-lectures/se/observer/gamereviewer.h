#ifndef __GAMEREVIER_H__
#define __GAMEREVIER_H__
#include "website.h"
#include "article.h"
#include <string>

struct GameReviewer : public Website{
    Article latest;
    std::string site;
    GameReviewer(std::string site);
    void review(std::string game);
    void notifySubs();
};

#endif
