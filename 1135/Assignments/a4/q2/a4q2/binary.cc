/***********************************************************************
*  CS 246 Assignment 4 Question 2
*
*
*  file: binary.cc
*  Definitions for methods in Binary class, derived class of Expression 
************************************************************************/

#include <iostream>
#include <string>
#include "binary.h"
using namespace std; 

// constructor
Binary::Binary():
	Expression(),
	op('\0'),
	left(NULL),
	right(NULL) {}

// param constructor
Binary::Binary(  );  

// copy constructor
Binary::Binary( const char op, Expression* left, Expression* right ): op(op),
																							 left(left), 
																							 right(right) {evaluate();} 

// operator= 
Binary& Binary::operator=(const Binary &rhs) {
	if (this == &rhs) return *this;
	Expression::operator=(rhs); //implicit slicing
	op = rhs.op; 
	left = rhs.left; 
	right = rhs.right;
	return *this;
};

void Binary::prettyprint(){
	cout << getInfix() << endl;
	cout << "= " << getValue() << endl;
}      

void Binary::evaluate(){
	switch(op){
		case '+':
			setValue ( left->getValue() + right->getValue() );
			setInfix ( "(" + left->getInfix() + " + " + right->getInfix() + ")" );
			break;
		
		case '-':
			setValue ( left->getValue() - right->getValue() );
         setInfix ( "(" + left->getInfix() + " - " + right->getInfix() + ")" );
         break;
      
		case '*':
			setValue ( left->getValue() * right->getValue() );
         setInfix ( "(" + left->getInfix() + " * " + right->getInfix() + ")" );
         break;
      
		case '/':
		   setValue ( left->getValue() / right->getValue() );
         setInfix ( "(" + left->getInfix() + " / " + right->getInfix() + ")" );
	      break;
	} 
} 
