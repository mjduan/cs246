#include "rationalo.h"
using namespace std;

Rational operator+(const Rational& a, const Rational& b){
    Rational c;
    if ( a.denominator == b.denominator){
        c.numerator = (a.numerator + b.numerator);
        c.denominator = b.denominator;
    } else {
        c.numerator = (a.numerator * b.denominator) + (b.numerator * a.denominator);
        c.denominator = (a.denominator * b.denominator);
    }
    return c;
}
Rational operator+(const Rational& a, const int& b){
        Rational c (a.numerator + (b * a.denominator), a.denominator);
        return c;
}

istream& operator>>(istream& is, Rational &in_rational){
    is >> in_rational.numerator >> in_rational.denominator;
    return is;
}
ostream& operator<<(ostream& os, const Rational &out_rational){
    os << out_rational.numerator << "/" <<  out_rational.denominator;
    return os;
}

