#include <iostream>
#include <string>
#include <sstream>
#include "expression.h"
#include "integer.h"
#include "unary.h"
#include "binary.h"
#include "stack_arr.h"
using namespace std; 

int main (){
	// creates new stack for reading expression 	
	stack* s = new stack;
 
	while( !cin.eof() ){
		string command; 
		cin >> command; 
		
		stringstream buffer(command); 
		long int i;
		
		if (buffer>>i){			 			 
			//cout << "!----input is num. Value is: " << i << endl;
			Expression* newInt = new Integer(i);				
			push(s, newInt);
			//printStack(s);	 
		} 
		
		if (command == "+" || command == "-" || command == "*" || command == "/"){
			// binary operator called
			if (s->size <= 1){
           	break;
        	}
			//cout << "!----input is Binary: " << command << endl;
			Expression* right = pop(s);
			//cout << "!---value of right is: " << right->getValue()  << endl; 
			Expression* left = pop(s);
			//cout << "!---value of left is: "<< left->getValue() << endl;
        	if ( right == NULL || left == NULL ){
				//cout << "!--- illegal call, not enough items" << endl; 
           	break;
        	}
        	Expression* newBin = new Binary(command[0], left, right);
        	//cout << "!---value of newBin is: " << newBin->getValue() << endl; 
        	push(s, newBin);			 
			//printStack(s); 
		}
		// checks for Unary operator 
		if(command == "ABS" || command == "NEG"){
			Expression* target = pop(s); 
		   //cout << "!---- Value of target is: " << target->getValue() << endl;
        	Unary* newUni = new Unary(target, command);
			//cout << "!---- Value of newUni is: " << newUni->getValue() << endl;
			//cout << "!---- newUni infix notation is: " <<  newUni->getInfix() << endl; 
     		//cout << "!---- newUni Operator is: " << newUni->getOp() << endl; 
      	Expression* temp = newUni; 
			push(s, temp);
		}
	}
	Expression* finalObject = pop(s); 
	// if nothing on stack, pop will return null 
	// check for this and end program if so 
	if (finalObject == NULL) return 0; 
	finalObject->prettyprint();   
	delStack(s);  
   return 0; 
}

