/***********************************************************************
*
* CS 246 Assignment 5 *
*
*  file: cell.cc
*  Definitions of methods for Cell class, individual cells in the Grid
************************************************************************/
#include <iostream>
#include "cell.h"
using namespace std; 

// Default constructor
Cell::Cell(): fill(NULL),
              numNeighbours(0),
              r(0), c(0) {}  

// Explicitly sets cell to living.
void Cell::setFill(char c){
	//cout << "!====setLiving called" << endl;  
      fill = c;     
	//cout << "!--- isAlive is now: " << isAlive << endl; 
}         

void Cell::setCoords(int r, int c){
	this->r = r; 
	this->c = c; 
}

void Cell::notifyDisplay(TextDisplay &t){
	//cout << "!====Cell::notifyDisplay called" << endl; 
	//cout << "	 -cell is" << r << " x " << c << ", isAlive:" << isAlive  << endl; 
	//cout << "ch is now: " << ch << endl;
   t.notify(r, c, fill);
}
 
void Cell::addNeighbour(Cell *neighbour){
   if (numNeighbours >= maxNeighbours) return; 
   
   neighbours[numNeighbours] = neighbour; 
   numNeighbours++; 
}

/*
// My neighbours will call this, to let me know they're alive.
void Cell::notify(){ 
	//cout << "!====cell:notify, cell: " << r << " x " << c << endl;
	//cout << "!---- liveNeigh is: "<< liveNeighbours << endl; 
	liveNeighbours += 1;
	//cout << "!---- liveNeigh is now: " << liveNeighbours << endl;  
}

/*
// Tell all of my neighbours that I am alive (if I'm alive).
void Cell::notifyNeighbours(){
	//cout << "!==== Cell:notifyNeighbours called" << endl; 	
   if ( isAlive ) {
	//cout << "	is alive" << endl;
       for (int i = 0; i < numNeighbours; ++i){
           neighbours[i]->notify(); 
       }
   }
} 
/*
// Reassess my living-or-dead status, based on info from neighbours.
void Cell::recalculate(){
   // if number of live neighbours <2 or >3 then Cell dies
   if ( isAlive && (liveNeighbours < 2 || liveNeighbours > 3) ){ 
		//cout << "!==== Cell::recalculate case 1)" << endl;  
		isAlive = false; 
	} else if ( !isAlive && (liveNeighbours == 3) ) {
		//cout << "!==== Cell::Recalculate case 2)" << endl; 
		isAlive = true; 
	} 
	
   // resests live neighbours for next round of calculation
   liveNeighbours = 0; 
}       
*/