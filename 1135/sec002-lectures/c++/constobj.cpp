struct Rational{
    int numer, denom;
    Rational(int n=0, int d=1) : numer(n), denom(d) {}
    double toDouble(){
        return (double)numer/denom;    
    }
    double consttoDouble() const{
        return (double)numer/denom;
    }
};

int main(){
    const Rational r(1,2);
    r.numer = 5; // gives error
    r.denom = 7; // gives error
    double val = r.toDouble(); // gives error
    double val2 = r.consttoDouble(); // works
}
