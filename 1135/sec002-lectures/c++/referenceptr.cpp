#include <iostream>
using namespace std;

int foo(int &x){
    x += 7;
    return x + 2;
}

int bar(const int& x){
    // x += 7;
    return x + 2;
}

int main(){

    int x = 42;
    int &y = x;
    const int& z = x; // cannot change x's value
    int *px = &x;
    y = 32;
    cout << "x: " << x << endl;
    x = 42;
    cout << "y: " << y << endl;
    cout << boolalpha <<  "Do x and y have the same address? " << (&x == &y) << endl;
    cout << "Is address of x, value of px?: " << (&x == px) << endl;
    cout << "Value of x, same as *px? : " << (*px == x) << endl;
    cout << "Value of x: " << x << endl;
    cout << "Reference parameter call return: " << foo(x) << endl;
    cout << "Value of x after call: " << x << endl;
    cout << "Value of x: " << x << endl;
    cout << "Const Reference parameter call return: " << bar(x) << endl;
    cout << "Value of x after call: " << x << endl;
    
    // Try to reassign z
    // z = 77;
}
