#include <string>

struct Database{
    static Database* singleton;
    static Database* getInstance();
    static void clean();
    unsigned int users;
    // maybe some internal database structures
    // ...
    Database();
    void addUser(std::string id);
    unsigned int getCount();
};
