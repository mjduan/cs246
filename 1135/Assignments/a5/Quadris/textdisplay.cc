/***********************************************************************
*  CS 246 Assignment 4 Question 3
*
*
*  file: textdisplay.cc
*  Definitions for methods in Textdisplay class
************************************************************************/

#include <iostream>
#include "cell.h"
using namespace std; 

// parameter constructor, creates text display of size n 
TextDisplay::TextDisplay(){
   theDisplay = new char*[dRows];
	//cout << "text display constructor called" << endl; 	    
   for( int r = 0; r < dRows; ++r ){
      theDisplay[r] = new char[]; 
      for ( int c = 0; c < dCols; ++c ) {
         theDisplay[r][c] = '_'; 
      }
   }
}

void TextDisplay::notify(int r, int c, char ch) {
	//cout << "!==td::notify() called" << endl; 
	//cout << "	--cell is: " << r << " x " << c << endl; 
	//cout << "	--char is: " << ch << endl;  
   theDisplay[r][c] = ch;
}

TextDisplay::~TextDisplay(){
  for( int r = 0; r < dRows; ++r ){      
      delete [] theDisplay[r];
   }
   delete [] theDisplay; 
}

ostream &operator<<(ostream &out, const TextDisplay &td){
	int r = 0;
	// prints out up to the last row
   for (; r < td.dRows-1; ++r){
		for (int c = 0; c < td.dCols; ++c){
			out << td.theDisplay[r][c];
		}
		out << endl;
   } 
	// print out last row
	for (int c = 0; c < td.dCols; ++c){
		out << td.theDisplay[r][c];
	}
	
	return out;
}
