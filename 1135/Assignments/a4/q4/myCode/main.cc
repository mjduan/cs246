#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
using namespace std;

#include "textprocess.h"
#include "echo.h"
#include "swapcase.h"
#include "doublevowels.h"
#include "skipword.h"
#include "translate.h"

int main () {
  string s;

  while(1) {
    //cout << "***loop started" << endl;
    getline(cin,s);
    if (cin.fail()) break;
    istringstream iss(s);
    string fname;
    iss >> fname;
    istream *in = (fname == "stdin") ? &cin : new ifstream(fname.c_str());
	 
    TextProcessor *tp = new Echo;
    //cout << "***tp created" << endl;
	string dec;
    while (iss >> dec) {
      if (dec == "swapcase") {
		//cout << "***dec == swapcase" << endl;
		tp = new SwapCase(tp, in);
      }
      else if (dec == "doublevowels") {
	//cout << "***dec == doublevowels" << endl;
        //TextProcessor *temp = tp;
	tp = new DoubleVowels(tp, in);
	//delete temp;
      }
      else if (dec == "skipword") {
        tp = new SkipWord(tp, in);
      }
      else if (dec == "translate") {
        char from, to;
        iss >> from >> to;
        // tp = ...
      }
   } 

   tp->setSource(in);

   string word;

   while (word = tp->getWord(), !tp->fail()) {
     cout << word << endl;
   }

   if (in != &cin) delete in;

   delete tp;
  }
}
