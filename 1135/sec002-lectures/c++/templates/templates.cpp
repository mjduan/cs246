#include <iostream>
using namespace std;

template<typename T=int, unsigned int N=10> // default type/value
class Stack{
    T elems[N];
    unsigned int size;
  public: 
    Stack() : size(0){}
    T top(){return elems[size - 1];}
    void push(T e){elems[size++] = e;}
    T pop(){return elems[size--];}
};

int main(){
    Stack<> si; // use defaults - still need <>
    si.push(3);
    si.push(4);
    cout << si.top() << endl;
    Stack<double> sd;
    sd.push(5.1);
    sd.push(5.2);
    Stack< Stack<int, 10> > ssi; // Note that > > must appear like this
    ssi.push(si);
    ssi.push(si);

}
