#ifndef __TACODECORATOR_H__
#define __TACODECORATOR_H__
#include "taco.h"


class TacoDecorator : public Taco{
  protected:
    Taco &t;
  public:
    TacoDecorator(Taco &t);
};

#endif
