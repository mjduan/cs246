#ifndef __UNARY_H__
#define __UNARY_H__

#include <iostream>
#include <string>
#include "expression.h"

class Unary: public Expression{
   Expression* expr; 
   std::string op; 
   
   public: 
      Unary(); // default const 
      Unary( Expression* target, std::string op );  // param constructor
      Unary( const Unary& target ); // copy const
      Unary& operator=(const Unary& rhs); // operator=
      
      void prettyprint();
      void evaluate();
      
      std::string getOp(); 
      
      Expression* getExpr();
      
};


#endif 
