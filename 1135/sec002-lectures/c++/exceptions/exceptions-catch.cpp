#include <exception>
#include <iostream>
using namespace std;
struct myexcept : public exception{
  const char* what() { return "myexcept";}
};
void rtn(){
   throw myexcept(0);
}

int main(){
  try{
     rtn();
  } catch (exception e){
     cout << "caught: " << e.what() << endl;
  }
}
