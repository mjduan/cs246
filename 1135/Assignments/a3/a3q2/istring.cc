/**********************************************************************************
/	CS 246: Assignment 3 Part 2
/
/
/	Question 2: istring.cc
/		Implementation Improved String Object, iString
/     supports overloaded >>, <<, + and * for iString
***********************************************************************************/
#include <iostream>
#include <cstring>
#include <locale>
#include "istring.h"
using namespace std; 

const int initCap = 10;

/*
struct iString{
	char * chars;
	unsigned int length;
	unsigned int capacity;
	iString();
	iString(const char *);
	iString(const iString&);
	~String();
};
*/

std::istream& operator>>(std::istream& in, iString& iS){  
   char c;  
	int i = 0;
	int count = 1; 
	
	// reads in first non-space char
	in >> c; 
	//cout << count << ")---" << "!c is now {" << c << "}" << endl;
   iS.chars[i] = c; 
   iS.length++; 
   i++; 
   c = in.peek(); 
   
	while (!isspace(c)){ 
	   //cout << count << ")---" << "!--peek c is now {" << c << "}" << endl;
	   if (iS.length > iS.capacity - 1) {// if size >= capacity, create new array double the size
	      //cout << count << ")---" << "!------- Double capacity called." << endl;
	      
	      char* tempArr = new char[2 * iS.capacity];

	      for (int j = 0; j < iS.length; j++){ // copy everything from iS.chars to temp array
	         tempArr[j] = iS.chars[j];  
	      }

         //cout << count << ")---" << "!------- temp arr is now: " << tempArr << endl;   
	      delete [] iS.chars; // delete old array
         iS.capacity *= 2; 
         
         //cout << "!------- new capacity is now: " << iS.capacity << endl;
	      
	      iS.chars = new char[iS.capacity];
	      iS.chars = tempArr; // reassign iS.chars to temp array 
	      
	      //cout << count << ")---" << "!------- capacity is now: " << iS.capacity << endl;  
	   }
      in >> iS.chars[i]; 
      //cout << "!---chars[" << i << "] is now: " << iS.chars[i] << endl;
      i++;
      iS.length += 1;    
      c = in.peek();        
      //cout << count << ")---" << "!---chars is now: " << iS.chars << endl;
      //cout << count << ")---" << "!---length is: " << iS.length << endl; 
      //cout << count << ")---" << "!---cap is : " << iS.capacity << endl;
   
      count++;     
   } 
   //cout << "!----chars is now: " << iS.chars << endl;
   //cout << "!----length is now: " << iS.length << endl;
   //cout << "!----capacity is now: " << iS.capacity << endl;
   return in;
}

std::ostream& operator<<(std::ostream& out, const iString& iS){
   cout << iS.chars;
   return out;
}

// default constructor
iString::iString(){ 
   //cout << "!---- Default constructor called" << endl; 
	
	this->length = 0; 
   this->capacity = initCap;
   this->chars = new char[initCap];
   
}

// constructor with string parameter
iString::iString( const char* str ){ 
	//cout << "!---- Param constructor called" << endl;
	//cout << "!---- The string is: " << str << endl;

   int count = 0;
   for ( ; str[count] != '\0'; count++ ){}
	
	//cout << "!---- count is: " << count << endl;   
   
   this->length = count;  
   this->capacity = count; 
   this->chars = new char[count];
	
	for (int i = 0; i < count; i++){
      this->chars[i] = str[i];
   }
	//cout << "!---- this->chars is now: " << this->chars << endl;
}
                                   
// copy constructor
iString::iString(const iString& iS){ 
   //cout << "!---- copy constructor called" << endl; 
	
	this->length = iS.length; 
   this->capacity = iS.capacity;
   
     // deep copy of cpy
   this->chars = new char[capacity];
   for( unsigned int i = 0; i < iS.length; ++i){
      //cout << "!--- iS["<< i << "] is: " << this->chars << endl;
		
		this->chars[i] = iS.chars[i];
		
		//cout << "!--- new char[" << i << "] is: " << this->chars[i] << endl;
   }
}

// destructor
iString::~iString(){
	//cout << "!---- Destructor called" << endl;
   delete [] this->chars; 
}



// method definition for iS + iS
iString operator+(const iString& iS1, const iString& iS2){
	//cout << "!---- op+(iS, iS) called" << endl; 
	iString* retIs = new iString; // iS to be returned
	int i = 0;
	unsigned int newLen = iS1.length + iS2.length; 
	unsigned int newCap = iS1.capacity + iS2.capacity; 
	
	char* tempArr = new char[newCap]; // temporary array
   
   for ( ; i < iS1.length; i++){
      tempArr[i] = iS1.chars[i];
      //cout << "i is: " << i << endl;
      //cout << "!--- tempArr is now: " << tempArr << endl;          
   } 

   //cout << "!-i is now: " << i << endl; 

   for ( int j = 0; j < iS2.length; j++){
      tempArr[i] = iS2.chars[j];
      //cout << "j is: " << j << endl;
      //cout << "!--- tempArr is now: " << tempArr << endl;
      i++;
   } 

   //cout << "!----tempArr is now: " << tempArr << endl; 

   retIs->length = newLen;
   retIs->capacity = newCap;
   retIs->chars = tempArr; // new object to be returned 
	return *retIs;
}


iString operator+(const iString& iS, const char* str){
  	//cout << "!---- op+(iS, str) called" << endl; 
  	iString* retIs = new iString;  
   int newLen, newCap, i; 
   
   //cout << "!--- strlen is: "<< strlen(str) << endl;
   newLen = iS.length + strlen(str); 
   //cout << "!-- newLen is: " << newLen << endl;
   
   if ( newLen > iS.capacity ) { newCap = newLen; }
   else { newCap = iS.capacity; }
   
   //cout << "!-- newCap is: " << newCap << endl;
   char* tempArr = new char[newLen];
   i = 0; 
   
   for ( ; i < iS.length; i++ ) {
      tempArr[i] = iS.chars[i]; 
      //cout << "i is: " << i << endl;
      //cout << "!--- tempArr is now: " << tempArr << endl;     
   }
   
   //cout << "!-i is now: " << i << endl; 

   for ( int j = 0; j < strlen(str); j++){
      tempArr[i] = str[j];
      //cout << "j is: " << j << endl;
      //cout << "!--- tempArr is now: " << tempArr << endl;
      i++;
   } 
      
   retIs->length = newLen; 
   retIs->capacity = newCap; 
   retIs->chars = tempArr; 
	return *retIs;
}

iString operator*(const iString& iS, const int n){
   //cout << "!---- op*(iS, Int) called";
   iString* retIs = new iString; 
   int newLen, newCap, i; 
   
   newLen = iS.length * n; 
   if ( newLen > iS.capacity ) { newCap = newLen; }
   else newCap = iS.capacity; 
   
   char* tempArr = new char[newLen];
   
   i = 0; 
   for ( int j = 0; j < n; j++ ) {
      for ( int k = 0 ; k < iS.length; k++ ) {
         tempArr[i] = iS.chars[k]; 
         i++;
         //cout << "i is: " << i << endl;
         //cout << "!--- tempArr is now: " << tempArr << endl;     
      }
   }
   //cout << "!-i is now: " << i << endl; 

   retIs->length = newLen; 
   retIs->capacity = newCap; 
   retIs->chars = tempArr; 
	return *retIs;
}

iString operator*(const int n, const iString& iS){
	return iS * n; 
}


