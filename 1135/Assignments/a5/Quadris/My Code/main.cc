/***********************************************************************
*  CS 246 Assignment 4 Question 3
*
*
*  file: main.cc
*  Main harness for a4q3 
************************************************************************/

#include <iostream>
#include <string>
#include <sstream>
#include "cell.h"
#include "grid.h"
#include "textdisplay.h"
using namespace std; 

int main() {
	cout << "Sucessful Compilation" << endl;
	Grid g = Grid(); 
	// commands 
	string left = "left";
	string right = "right";
	string down = "down"; 
	string drop = "drop";
	string cw = "clockwise";
	string ccw = "counterclockwise";
	string lvUp = "levelup"; 
	string lvDown = "leveldown";
	string restart = "restart"; 

	while ( !cin.eof() ){
		stringstream buffer;
		string command, comm;
		char ch;
		int n, r, c, mult; 
		mult = 1; n = 0; r = 0; c = 0;

		getline(cin, command);
		buffer << command; // read command into sstream
		
		// checks for multiplier 
      buffer >> mult;
      if ( buffer.fail() ){
         cout << "no mult " << mult << endl;
         buffer.clear();
         mult = 1; // reset mult to 1 
      }
		// read first while space deliminated string from command

		buffer >> comm; 
		cout << "   [comm is: " << comm << " [length is: " << comm.size()  << endl;
      if (comm.size() <= 1) continue;

		if ( comm ==  "new" ){
			cout << "comm is: " << comm << endl;
			//buffer >> n; // read in dimension 
			//cout << "n is: " << n << endl;
			g.init(); 
			//g.clearGrid(); 	 
		}

		if ( comm == "init" ){
			while (true){
				cout << "init called" << endl;
				cin >> r >> c;
				if (r == -1 && c == -1){ break; } 
				cin >> ch; 
				g.setFill(r,c,ch);  
			}
		}
		
		if ( comm == "step" ){
			buffer >> n;
  
			if (n == 0) {
				g.tick();  
			}else if ( n >= 0 ) {
				for (int i = 0; i <= n; ++i ){
					g.tick();	
				}
			}  
		} 

		if ( comm == "print"){
			cout << g << endl;
		}
////////////////////////////////////////////////////////////////////////////////
		// new commands for Quardis
		 
		// check if command is left
      if ( comm.size() <= left.size() ){
         if ( ( left.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 3 ){
            for ( int i = 0; i < mult; ++i){
               cout << "!==left detected, executing function calls" << endl;
            }
         }
      }
      if ( comm.size() <= right.size() ){
         if ( ( right.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            for (int i = 0; i < mult; ++i) {
               cout << "!==right detected, executing function calls" << endl;
            }
         }
      }
      if ( comm.size() <= down.size() ){
         if ( ( down.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            for (int i = 0; i < mult; ++i){
               cout << "!==down detected, executing function calls" << endl;
            }
         }
      }
      if ( comm.size() <= drop.size() ){
         if ( ( drop.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            for (int i = 0; i < mult; ++i){
               cout << "!==drop detected, executing function calls" << endl;
            }
         }
		}
		if ( comm.size() <= cw.size() ){
         if ( ( cw.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            for (int i = 0; i < mult; ++i){
               cout << "!==cw detected, executing function calls" << endl;
            }
         }
      }
      if ( comm.size() <= ccw.size() ){
         if ( ( ccw.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            for (int i = 0; i < mult; ++i){
               cout << "!==ccw detected, executing function calls" << endl;
            }
         }
      }
      if ( comm.size() <= lvUp.size() ){
         if ( ( lvUp.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 6 ){
            for (int i = 0; i < mult; ++i){
               cout << "!==lvUp detected, executing function calls" << endl;
            }
         }
      }
      if ( comm.size() <= lvDown.size() ){
         if ( ( lvDown.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 6 ){
            for (int i = 0; i < mult; ++i){
              cout << "!==lvDown detected, executing function calls" << endl;
            }
         }
      }
		if ( comm.size() <= restart.size() ){
         if ( ( restart.compare( 0,comm.size(),comm ) == 0 ) && comm.size() >= 2 ){
            cout << "!==restart detected, executing function calls" << endl;
         }
      }
   }
	return 0;
}
