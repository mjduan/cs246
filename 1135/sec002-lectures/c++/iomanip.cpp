#include <iostream>
#include <iomanip>
using namespace std;
int main(){
  bool b = false;
  cin >> b;
  if (cin.fail()){
    cerr << "Not a 0/1" << endl;
    cin.clear();
    // Try and see if they gave true/false
    cin >> boolalpha >> b;
    if(cin.fail()){
        cerr << "Not true/false" << endl;
        cin.clear();
        cin.ignore();
    } else {
        cout << boolalpha << "Boolean given: " << b << endl;
    }
  } else {
    cout << "Boolean given: " << b << endl;
  }
  
  int hex_num;
  int dec_num;
  cin >> hex >> hex_num;
  cin >> dec >> dec_num;

  cout << "hex in: " << hex_num << hex << " dec in: " << dec_num << endl;

  double fp_num= 3.14159265359;
  cout << "Default: " << fp_num << setprecision(10) << " Modified: "<< fp_num << endl;
}
