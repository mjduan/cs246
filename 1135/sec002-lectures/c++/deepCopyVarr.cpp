#include <iostream>
using namespace std;
struct Varr{
    int size;
    int *array;
    Varr(){
        array = new int[10];
        size = 10;
        for(int i=0; i<10; ++i){
            array[i] = i;
        }
    }
    Varr(const Varr& cpy){
        size = cpy.size;
        array = new int[size];
        for(int i=0; i < size; ++i){
            array[i] = cpy.array[i];
        }
    }
    ~Varr(){
        if( array) delete [] array;
    }
};

void printVarr(const Varr& arr){
    for(int i=0; i < arr.size; ++i){
        cout << arr.array[i] << " ";    
    }
    cout << endl;
}

int main(){
    Varr arr1;
    Varr arr2 = arr1;
    printVarr(arr1);
    printVarr(arr2);
    arr1.array[5]=42;
    printVarr(arr1);
    printVarr(arr2);
}

