#include "lettuce.h"
#include <iostream>
using namespace std;

Lettuce::Lettuce(Taco& t) : TacoDecorator(t){}
double Lettuce::cost() { return t.cost() + 0.50;}
void Lettuce::desc() { t.desc(); cout << ", lettuce";}
