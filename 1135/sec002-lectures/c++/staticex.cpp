#include <iostream>
using namespace std;
struct Foo{
    static int cnt; // one for all objects (kind of like a global but restricted to an object)
    int i; // one per object
    void mem(){ i = 7; }
    static void stats(){
        cout << cnt << endl; // allowed
        //i = 3; // disallowed
        //mem(); //disallowed
    } 
    Foo(){
        cnt +=1; //allowed
        i = 5; //allowed
        stats(); //allowed
    }
};

int Foo::cnt = 0; // initialization optional
int main(){
    Foo x,y;
}
