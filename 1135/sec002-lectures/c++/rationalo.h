#ifndef __RATIONALO_H__
#define __RATIONALO_H__
#include <iostream>
// No using namespace in a .h file. Bad to do so
struct Rational{
    int numerator, denominator;
    Rational(int n = 0, int d = 1) {
        numerator = n;
        denominator = (d != 0) ? d : 1;
    }
    Rational(const Rational& b){
        numerator = b.numerator;
        denominator = b.denominator;   
    }
    double toDouble() const{
        return (double)numerator/denominator;
    }
};
// Routine declarations
Rational operator+ (const Rational& a, const Rational& b);
Rational operator+ (const Rational& a, const int& b);

// Other arithmetic operators ...
std::ostream& operator<<(std::ostream& os, const Rational& rat_out);
std::istream& operator>>(std::istream& is, Rational& rat_in);
#endif

