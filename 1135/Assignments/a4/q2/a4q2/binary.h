#ifndef __BINARY_H__
#define __BINARY_H__

#include <iostream>
#include <string>
#include "expression.h"

class Binary: public Expression{
	char op;    
	Expression* left;
	Expression* right; 
   
	public: 
		Binary(); // default constructor 
		Binary( const char op, Expression* left, Expression* right );  // param constructor
		Binary( const Binary& ); // copy constructor
		Binary& operator=( const Binary &rhs ); // operator= 
         
		void prettyprint();      
		void evaluate(); 
};

   
#endif 
