#ifndef __SCOREBOARD_H__
#define __SCOREBOARD_H__
#include <string>

class Scoreboard{
   static int gameNum; 
   static int counter; 
   int maxDeduct; 
   
   Player player1; 
   Player player2; 

};

struct FTree {
   // fields
   std::string name;
   int numChildren;
   FTree *children[maxChildren]; 
   //methods 
	FTree();
   FTree(const std::string str);
   FTree(const FTree& tree);
   void swap(FTree& rhs);
   FTree& operator=(const FTree& rhs);
   ~FTree();
};

void print( const FTree& tree);
bool addNode( FTree& tree, const std::string parent, const std::string child );
FTree* ancestor( FTree& root, const std::string ancestor );
std::string nodePath(FTree& node, const std::string name, std::string ret);
void delNode(FTree& tree, const std::string target); 

#endif
