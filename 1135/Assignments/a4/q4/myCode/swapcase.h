#ifndef __SWAPCASE_H__
#define __SWAPCASE_H__
#include <iostream>
#include <string>
#include "textprocess.h"
#include "decorator.h"

class SwapCase: public Decorator{
	std::istream *source;
	bool failed;
	
	public:
		void setSource(std::istream *inp); 
		std::string getWord();
		bool fail () const;
		SwapCase(TextProcessor *tp, std::istream* in);
		~SwapCase(); 
};

#endif
