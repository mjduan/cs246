#ifndef __ISTRING_H__
#define __ISTRING_H__

struct iString{
	char * chars;
	unsigned int length;
	unsigned int capacity;
	iString();
	iString(const char *);
	iString(const iString&);
	~String();
};

// Add other declarations here:

#endif
