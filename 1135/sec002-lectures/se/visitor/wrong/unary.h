#ifndef __UNARY_H__
#define __UNARY_H__
#include "tree.h"

class Unary : public Tree{
  protected:
    Tree * child;
  public:
    void accept(Visitor& v);
    Unary(std::string, Tree *);
    ~Unary();
};

#endif
