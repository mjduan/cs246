#ifndef __BINARY_H__
#define __BINARY_H__
#include "tree.h"

class Binary : public Tree{
  protected:
    Tree * left, * right;
  public:
    void accept(Visitor& v);
    Binary(std::string, Tree * t1, Tree *t2);
    ~Binary();
};

#endif
