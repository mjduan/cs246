#ifndef __RATIONALCLASS_H__
#define __RATIONALCLASS_H__
#include <iostream>
// No using namespace in a .h file. Bad to do so
class Rational{
      int numerator, denominator;
      friend Rational operator+ (const Rational& a, const Rational& b);
      friend Rational operator+ (const Rational& a, const int& b);
      friend std::ostream& operator<<(std::ostream& os, const Rational& rat_out);
      friend std::istream& operator>>(std::istream& is, Rational& rat_in);
    public:
      Rational(int n = 0, int d = 1);
      Rational(const Rational& b);
      double toDouble() const;
      double getNumer() const;
      double getDenom() const;
      void setNumer(int n);
      void setDenom(int d);
};
// Routine declarations
Rational operator+ (const Rational& a, const Rational& b);
Rational operator+ (const Rational& a, const int& b);

// Other arithmetic operators ...
std::ostream& operator<<(std::ostream& os, const Rational& rat_out);
std::istream& operator>>(std::istream& is, Rational& rat_in);
#endif

