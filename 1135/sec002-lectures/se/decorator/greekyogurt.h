#ifndef __GREEKYOGURT_H__
#define __GREEKYOGURT_H__
#include "tacodecorator.h"


class GreekYogurt : public TacoDecorator{
  public:
    GreekYogurt(Taco& t);
    double cost(); 
    void desc();
};
#endif
