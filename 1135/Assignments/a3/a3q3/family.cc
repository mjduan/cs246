/*******************************************************************
*  CS 246 Assignment 3 Question 3
*
*
*  file: family.cc
*  Descendent based family tree implemented as C++ object
********************************************************************/
#include <iostream>
#include <fstream>
#include <locale>
#include <string>
#include "family.h"

using namespace std; 

// ancestor: adds name as ancestor to tree
FTree* ancestor( FTree& root, const string ancestor ){
  
   FTree* retTree = new FTree; 
   char c; 

 
   retTree->name = ancestor; 
    
   retTree->children[0] = &root; 
     
   retTree->numChildren += 1;    
      
   
   return retTree;
}

// addNode: FTree + string + string --> FTree 
bool addNode( FTree& tree, const string parent, const string child ){
   
   
   if (&tree == 0){ return false; } // base false case 
   
   if ( tree.name == parent ) { 
      if ( tree.numChildren >= 10 ){ return false; }

      FTree* newChild = new FTree;      
      newChild->name = child; 
      tree.children[tree.numChildren] = newChild; 
      tree.numChildren++; 
      return true;
   }
   for ( int i = 0; i < tree.numChildren; i++ ){
     
      if ( addNode(*tree.children[i], parent, child) ) { return true; }
   }
   return false; 
}

void print( const FTree& tree ){
   //cout << "!---- PRINT called" << endl;   
   int numC = tree.numChildren; 

   if (numC > 0) {
      cout << tree.name << " is parent of "; 
      
      for (int i = 0; i < numC-1; i++){
         cout << tree.children[i]->name << ","; 
      }
      
      cout << (*tree.children[numC-1]).name << endl;
         
      for (int i = 0; i < numC; i++){    
         print(*(tree.children[i])); 
      }    
   }
}

// default constructor
FTree::FTree(){
  
   this->name = " ";
   this->numChildren = 0; 
   for (int i = 0; i < maxChildren; i++){
      this->children[i] = NULL;
   }
  
}

// parameter constructor
FTree::FTree(const string str){
   
   this->name = str;
   this->numChildren = 0; 
   for (int i = 0; i < maxChildren; i++){
      this->children[i] = NULL;
   }
   
}

// destructor 
FTree::~FTree(){
   
   if (this->numChildren > 0){
      for (int i = 0; i < this->numChildren; i++){
         delete this->children[i];      
      }
   }
}

// copy constructor
FTree::FTree(const FTree& tree){

   this->name = tree.name; 
   this->numChildren = tree.numChildren; 

   for (int i = 0; i < tree.numChildren; i++){
      this->children[i] = tree.children[i];
   }

}

//swap method, used by copy constructor
void FTree::swap(FTree& rhs){

   string tempName = rhs.name; 
   int tempNum = rhs.numChildren; 
   FTree* tempChild[maxChildren];
   
   // copy rhs into temp 
   for (int i = 0; i < rhs.numChildren; i++) {
      tempChild[i] = rhs.children[i];
   }
   
   rhs.name = this->name; 
   this->name = tempName;
   rhs.numChildren = this->numChildren; 
   this->numChildren = tempNum; 


   for (int i = 0; i < maxChildren; i++) {
      rhs.children[i] = this->children[i];
   }
  
   for (int i = 0; i < maxChildren; i++) {
      this->children[i] = tempChild[i];
   }     
}

//operator=
FTree& FTree::operator=(const FTree& rhs){
   
   FTree temp = rhs;     
	swap(temp); 

   return *this;
}

// nodePath: FTree&, String --> String 
//    Returns a path from the root to the given name as a string
//    if node is not in FTree, return " "
string nodePath( FTree& node, const string name, string ret){
 
   if ( node.name == " " ){ return ret; }   

   if ( &node == 0 ) { 

      return ret; 
   }  

   if ( node.name == name ) {
      ret = node.name; 

      return ret; 
   } 

   for ( int i = 0; i < node.numChildren; i++ ){
      ret = nodePath(*node.children[i], name, ret);
      
      if ( ( ret != " ") ){

          ret = (node.name + "," + ret);
          return ret; 
      }
   } 
   return " ";
}

// FTree, String --> Void
//    Deletes String and it's children from FTree
//    if String is not present, it does nothing

void delNode(FTree& tree, const string target){
   
   // case for root.name = target 
   if ( tree.name == target ) {
      
      // delete all children 
      for (int i = 0; i < tree.numChildren; i++){
        delete tree.children[i]; 
      }
      // reset root to base root
      tree.name = " ";
      tree.numChildren = 0;     
      for (int i = 0; i < maxChildren; i++){
         tree.children[i] = NULL;
      }
   } 
   // check if children are target
   for (int i = 0; i < tree.numChildren; i++){
      if (tree.children[i]->name == target){
         
         delete tree.children[i];
         tree.numChildren -= 1; 
         
         // shift over array 
         for ( ; i < maxChildren - 2; i++ ){
        
            tree.children[i] = tree.children[i+1];         
         }
         tree.children[i+2] = NULL; 

         return;
      }
   }        
   // recursive call
   for (int i = 0; i < tree.numChildren; i++){
      delNode(*(tree.children[i]), target);
   }
}








