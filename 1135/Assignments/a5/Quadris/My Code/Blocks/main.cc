#include <iostream>
#include <string>
#include "block.h"
using namespace std;

int main() {
	cout << "Entering main function" << endl; 
	Block* b = new  Block(0,0);
	cout << "block created" << endl;  
	cout << *b; 
	delete b; 
	return 0; 
}
