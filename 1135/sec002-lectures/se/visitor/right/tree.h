#ifndef __TREE_H__
#define __TREE_H__
#include <string>

class Visitor;

class Tree{
  protected:
    std::string data;
    Tree & operator=(const Tree&);
  public:
    Tree(std::string);
    virtual void accept(Visitor& v) = 0;
    std::string getData();
    virtual ~Tree() = 0;
};
#endif
