#ifndef __TEXTDISPLAY_H__
#define __TEXTDISPLAY_H__
#include <iostream>
#include <sstream>

const int dRows = 15;
const int dCols = 10; 

class TextDisplay {
  char **theDisplay;
  //const int gridSize;
 public:
  TextDisplay();

  void notify(int r, int c, char ch);

  ~TextDisplay();

  friend std::ostream &operator<<(std::ostream &out, const TextDisplay &td);
};

std::ostream& operator<<(std::ostream &out, const TextDisplay &td);

#endif
