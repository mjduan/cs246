#include <sstream>
#include <iostream>
#include <string>
using namespace std;

int main() {
    double x;
    for(;;){
        cout << "Enter a floating point number: " ;
        string tmp;
        cin >> tmp;
        stringstream conv;
        conv.str(tmp);
        if ( conv >> x) break;
        cout << "Not a floating point number. Enter again!" << endl;
    }
    cout << "You have entered: " << x << endl;
}
