#include "printer.h"
#include "binary.h"
#include "unary.h"
#include "tree.h"
#include <iostream>
using namespace std;

void Printer::visit(Unary& u){
  cout << u.getData() << endl;
}

void Printer::visit(Binary & b){
  cout << b.getData() << endl;
}

Printer::~Printer(){}
