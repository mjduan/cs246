#ifndef __Visitor_H__
#define __Visitor_H__
class Binary;
class Unary;

class Visitor{
  public:
    virtual void visit(Unary &) = 0;
    virtual void visit(Binary &) = 0;
    virtual ~Visitor() = 0;
};
#endif
