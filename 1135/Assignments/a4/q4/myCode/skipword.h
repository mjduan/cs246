#ifndef __SKIPWORD_H__
#define __SKIPWORD_H__
#include <iostream>
#include <string>
#include "textprocess.h"
#include "decorator.h"

class SkipWord: public Decorator {
    std::istream *source;
    bool failed;
	 unsigned int counter; 
 public:
    SkipWord(TextProcessor *tp ,std::istream *in);
    void setSource(std::istream *inp);
    std::string getWord();
    bool fail() const;
    ~SkipWord();
};

#endif
