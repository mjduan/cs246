#include "emailfeed.h"
#include <iostream>
using namespace std;

void EmailFeed::notify(Article a){
    cout << "Article: \"" << a.name << "\"" << endl;
    cout << "Available at: " << a.url << endl;
    cout << "Summary: " << a.summary << endl;
    cout << endl;
}
