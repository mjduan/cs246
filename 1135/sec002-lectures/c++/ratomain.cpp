#include "rationalo.h"
#include <iostream>
using namespace std;

int main(){
    Rational a(1,2);
    Rational b(7, 5);
    Rational c = a + b;
    Rational d = a + 5;
    int x = 5;
    Rational e = a + x;
    // Implicit conversion from int to Rational
    Rational f = 7;
    // Implicit coversion from int to Rational
    // Allows operator+(Rational, Rational) to be called
    Rational g = x + a;
    Rational h = 5 + a;
    // Implicit conversion from double to int to Rational
    Rational i = 5.8;
    Rational j = 5 + 8; 
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    cout << "c: " << c << endl;
    cout << "d: " << d << endl;
    cout << "e: " << e << endl;
    cout << "f: " << f << endl;
    cout << "g: " << g << endl;
    cout << "h: " << h << endl;
    cout << "i: " << i << endl;
    cout << "j: " << j << endl;
    cout << 6+8 << endl;
}

