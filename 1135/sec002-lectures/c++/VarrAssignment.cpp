#include <iostream>
using namespace std;

struct Varr{
    int size;
    int *array;
    Varr(){
        array = new int[10];
        size = 10;
        for(int i=0; i<10; ++i){
            array[i] = i;
        }
    }
    Varr(const Varr& cpy){
        size = cpy.size;
        array = new int[size];
        for(int i=0; i < size; ++i){
            array[i] = cpy.array[i];
        }
    }
    ~Varr(){
        if(array) delete [] array;
    }
    Varr& operator=(const Varr& rhs){
        if (this == &rhs ) return *this; // Prevent self assignment
        delete [] array;
        size = rhs.size;
        array = new int[size];
        for(int i =0; i < size; ++i){
            array[i] = rhs.array[i];
        }
        return *this;
    }
    // Alternate Approach using copy and swap idiom
    // Note: No check for self-assignment. Why?
    /*
    void swap(Varr& rhs){
        int temp = rhs.size;
        rhs.size = size;
        size = temp;
        int* tp = rhs.array;
        rhs.array = array;
        array = tp;
    }
    Varr& operator=(const Varr& rhs){
        Varr tmp = rhs;
        swap(tmp);
        return *this;
    }
    */
};

void printVarr(const Varr& arr){
    for(int i=0; i < arr.size; ++i){
        cout << arr.array[i] << " ";    
    }
    cout << endl;
}

int main(){
    Varr arr1;
    Varr arr2;
    arr2 = arr1;
    printVarr(arr1);
    printVarr(arr2);
    arr1.array[5]=42;
    printVarr(arr1);
    printVarr(arr2);
    arr1 = arr1;
    printVarr(arr1);
}
