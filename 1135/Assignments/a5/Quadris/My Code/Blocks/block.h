#ifndef __BLOCK_H__
#define __BLOCK_H__

#include <iostream>
#include <string>

class Block{

	public:	

	char type; 
	int** array;
	int x, y; 
	public:
	Block();
	Block(int r, int c); // constructor
	~Block(); // destructor 
	//left(); 
	//right(); 
	//cw(); 
	//ccw(); 
	//drop(); 
	friend std::ostream& operator<<(std::ostream& out, const Block& b);  
};

std::ostream& operator<<(std::ostream& out, const Block& b); 
#endif
