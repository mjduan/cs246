#include "counter.h"

Counter::Counter() : count(0){}

void Counter::visit(Unary &u){ count++;}

void Counter::visit(Binary &b){ count++;}

Counter::~Counter(){}
