#ifndef __IMPRATIONALCLASS_H__
#define __IMPRATIONALCLASS_H__
#include "rationalclass.h"
// No using namespace in a .h file. Bad to do so
class  impRational : public Rational{
    int numCalls;        
  public:
    impRational();
    double toDouble();
    int calls();
};

#endif

