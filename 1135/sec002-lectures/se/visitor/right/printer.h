#ifndef __PRINTER_H__
#define __PRINTER_H__
#include "visitor.h"

class Printer: public Visitor{
  public:
    void visit(Unary &);
    void visit(Binary &);
    ~Printer();
};

#endif
