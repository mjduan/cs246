#include <iostream>
using namespace std;

int main(){
  int n;
  for (;;){
    cout << "Enter a number: ";
    if (! (cin >> n) ) {
      if (cin.eof()) break;
      cout << "Invalid number. Try again." << endl;
      cin.clear(); // reset stream failure
      // Ignore next character
      cin.ignore();
    } else {
      cout << "You entered: " << n << endl;
    }
  }
}
