#include <iostream>
using namespace std;
#include "rssfeed.h"

RSSFeed::RSSFeed(){
    for(int i=0; i < maxWebs; ++i) websites[i] = NULL;    
}

void RSSFeed::sub(Website * w){
    for(int i=0; i < maxWebs; ++i){
        if(websites[i] == NULL) {
            websites[i] = w;
            w->attach(*this);
            break;
        }
    }
}

void RSSFeed::unsub(Website * w){
    for(int i=0; i < maxWebs; ++i){
        if(websites[i] == w) {
            websites[i] = NULL;
            w->detach(*this);
            break;
        }
    }
}

RSSFeed::~RSSFeed(){
    for(int i=0; i < maxWebs; ++i){
        if(websites[i] != NULL) {
            websites[i]->detach(*this);
        }
    }
}

void RSSFeed::notify(Article a) { 
    cout << "Article: \"" << a.name << "\" ready to read." << endl;
    cout << "Summary: " << a.summary << endl;
    cout << "Link to article: " << a.url << endl;
    cout << endl;
}
