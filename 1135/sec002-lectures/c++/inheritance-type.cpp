#include <iostream>
using namespace std;

struct Foo{
    int i;
    double d;
}f;
struct Bar  : public Foo{
    char j;
}b;
void r(Foo f){ cout << f.i << endl << f.d << endl;}

int main(){
    f.i=1;
    f.d=2.2;
    b.i=7;
    b.d=3.3;
    b.j='a';
    r(f);
    r(b); // allowed
}
