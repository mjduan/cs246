#include <iostream>
using namespace std;
int r(int i, int j) { return 1;}

int r(int i) { return 2; } 

int r(double i, double j) { return 3;}

// double r(int i, int j) { return 4.2; }

int r(unsigned int i) { return 5;}

int main(){
    cout << r(1,1) << endl;
    cout << r(2) << endl;
    cout << r(3.3, 3.3) << endl;
    cout << r(-1) << endl;
    // Note: No cast makes the call ambiguous since it could call two r's
    // Not ambiguous for r(2) and r(-1) since both are valid signed ints
    // Compiler can only infer so much
    cout << r((unsigned int)4294967297) << endl;
} 
