#ifndef __DECORATOR_H__
#define __DECORATOR_H__
#include <iostream>
#include <string>
#include "textprocess.h"
class Decorator: public TextProcessor {
	protected:
		TextProcessor *component;
	
	public:
		Decorator(); // default constructor 	
		Decorator(TextProcessor *component); 
		virtual ~Decorator();
};

#endif
