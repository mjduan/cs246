/************************************************************
/ CS 246: Assignment 2 
/
/ Question 4 b): String parsing 
*************************************************************/


#include <iostream>
#include <string>
using namespace std;


int main (){
	int words; 
	string line;

	while ( !cin.eof() ){	// loop while not eof
		
		cin >> words; // read in number of words
//		cout << "words: " << words << endl;	

 
	  if ( words <= 0 || cin.fail() || !getline(cin,line) ){
		cin.clear();
		cin.ignore();
		continue; 	  
	  }
//	  cout << "line: " << line << endl;
//        cout << "line[0] is; " << line[0] << endl;
	    		

//		cout << "line[1] is; " << line[1] << endl;
//	        cout << "line[2] is; " << line[2] << endl;
//		cout << "len of line is: " << line.length() << endl;	
		int i; // counter for indexing the string 
		int flag = 0; 
		int count = 0; // count number of words

		for ( i = 0; i < line.length(); i++ ){

//			cout << line[i] << endl;
	
			if ( flag == 1 && (isspace(line[i]) || line[i] == '\t' )) {
				flag = 0;
			} else if (flag == 0 && !(isspace(line[i] || line[i] == '\t' ))) {
				flag = 1; 
				count++;
			} else continue;
			
	
//		cout << "count: " << count << endl ; 	

		}
	
	    if ( count == words) continue;
	
		cout << count << line << endl;  
	}
	return 0;
}
