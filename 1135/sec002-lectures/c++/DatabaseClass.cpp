#include "DatabaseClass.h"
#include <cstdlib>
using namespace std;

Database * Database::singleton = NULL;

Database::Database() : users(0){
    atexit(&clean);
}
Database* Database::getInstance(){
    if(singleton) return singleton;
    singleton = new Database;
    return singleton;
}
void Database::clean(){
    delete singleton;
}
void Database::addUser(string id){ users += 1;}

unsigned int Database::getCount() const { return users;}

