int foo(int i){
   if ( i <= 1 ) return 1;
   return i * bar(i-1);
}

int bar(int i){
   if ( i <= 1 ) return 1;
   return i * foo(i/2);
}

int main(){
   // Don't actually run this
   foo(42);
}
