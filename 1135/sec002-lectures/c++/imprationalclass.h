#ifndef __IMPRATIONALCLASS_H__
#define __IMPRATIONALCLASS_H__
#include "rationalclass.h"
// No using namespace in a .h file. Bad to do so
class  impRational : public Rational{
    int numCalls;        
  public:
    impRational() : numCalls(0){}
    double toDouble() { numCalls += 1; return Rational::toDouble();}
    int calls(){ return numCalls;}
};

#endif

