#include <iostream>
using namespace std;

struct Base{

    Base(){}
    ~Base() { cout << "~Base" << endl;}
};
struct Derived : public Base{

    Derived(){}
    ~Derived() { cout << "~Base" << endl;}
};

int main(){
    Base * bp = new Derived;
    // Allowable due to polymorphism
    delete bp;
}
