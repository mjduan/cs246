#ifndef __FAMILY_H__
#define __FAMILY_H__
#include <string>

const int maxChildren = 10;

struct FTree {
   // fields
   std::string name;
   int numChildren;
   FTree *children[maxChildren]; 
   //methods 
	FTree();
   FTree(const std::string str);
   FTree(const FTree& tree);

   void swap(FTree& rhs);
   FTree& operator=(const FTree& rhs);

   ~FTree();
};

void print( const FTree& tree);

bool addNode( FTree& tree, const std::string parent, const std::string child );

FTree* ancestor( FTree& root, const std::string ancestor );

std::string nodePath(FTree& node, const std::string name, std::string ret);

void delNode(FTree& tree, const std::string target); 

#endif
