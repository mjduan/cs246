#include <iostream>
using namespace std;

class Mushroom{
  protected:
    virtual void fill() = 0; 
  public:
    void draw(){ cout << "Drawing a basic mushroom outline." << endl; fill();}
};

class SuperShroom : public Mushroom{
  protected:
    void fill(){ cout << "Filling in the outline with red and white." << endl;}
};

class OneUpShroom : public Mushroom{
  protected:
    void fill(){ cout << "Filling in the outline with green and white." << endl;}
};

class PoisonShroom : public Mushroom{
  protected:
    void fill(){ cout << "Filling in the outline with shades of purple" << endl;}
};

int main(){
    Mushroom * m1 = new SuperShroom;
    m1->draw();
    Mushroom * m2 = new OneUpShroom;
    m2->draw();
    Mushroom * m3 = new PoisonShroom;
    m3->draw();
    SuperShroom ss;
    ss.draw();
    PoisonShroom ps;
    ps.draw();
    delete m1;
    delete m2;
    delete m3;
//    Pure virtual means Mushroom is abstract and can't instantiated
//    Mushroom m;
}
