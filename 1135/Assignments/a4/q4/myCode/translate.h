#ifndef __TRANSLATE_H__
#define __TRANSLATE_H__
#include <iostream>
#include <string>
#include "textprocess.h"
#include "decorator.h"

class Translate: public Decorator {
    std::istream *source;
    bool failed;
 public:
    Translate();
    void setSource(std::istream *inp);
    std::string getWord();
    bool fail() const;
    ~Translate();
};

#endif
