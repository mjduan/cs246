#ifndef __COUNTER_H__
#define __COUNTER_H__
#include "visitor.h"

class Counter: public Visitor{
  public:
    Counter();
    unsigned int count;
    void visit(Unary &);
    void visit(Binary &);
    ~Counter();
};

#endif
