#include <iostream>
using namespace std;

struct Vehicle{
    virtual void travel(){cout << "Travelling in a vehicle" << endl;}
};
struct Car : public Vehicle{
    virtual void travel(){cout << "Driving in a car" << endl;}
};

struct Boat : public Vehicle{
    virtual void travel(){cout << "Sailing in a boat" << endl;}
};

int main(){
    Vehicle * varr[10];
    for(int i = 0 ; i < 10; ++i){
        if( i % 2 == 0) varr[i] = new Car;
        else varr[i] = new Boat;
    }
    // Can treat all derived classes from Vehicle the same using 
    // Vehicle's interface
    for(int i = 0 ; i < 10; ++i){
        varr[i]->travel();
    }
    // Note the use of ptrs to Vehicle objects, consider
    Vehicle v = *new Car;
    v.travel();
    // Polymorphic value assignment just happened - an instance of slicing

}
