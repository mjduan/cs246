#include <iostream>
using namespace std;

struct Base{
    Base() { cout << "Base()" << endl;}  
    Base(const Base& o){ cout << "Base(Base&)" << endl;}
    Base& operator=(const Base& b){cout <<"Base =" << endl; return *this;}
};
struct Derived : public Base{
  int i; // basic type => bitwise copy  
  Derived(){ cout << "D()" << endl;}
  Derived(const Derived& d) : i(d.i), Base(d) {
    cout << "Derived(Derived&)" << endl;
  }
  Derived& operator=(const Derived& d){
    i = d.i;
    Base::operator=(d); // cast to base reference - coercion
    cout << "Derived =" << endl;
    return *this;}
};
int main(){
    Derived a;
    Derived b = a;
    b = a;
    Base c = a ;
    // Note Derived d = c; doesn't work
}
