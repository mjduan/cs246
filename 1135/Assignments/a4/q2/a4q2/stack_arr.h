#ifndef __STACK_ARR_H__
#define __STACK_ARR_H__

#include <iostream>
#include "stack_arr.h"
#include "expression.h"

const int MAX = 10;

struct stack {
	Expression* data[MAX]; 
   int size; 
};

stack* newStack();
void push(stack* s, Expression* item);
Expression* pop( stack* s );
Expression* top( stack* s);
void delStack( stack* s);
void printStack( stack* s); 
//bool peek ( stack* s );

#endif
