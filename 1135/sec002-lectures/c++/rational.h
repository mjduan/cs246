#ifndef __RATIONAL_H__
#define __RATIONAL_H__
#include <iostream>
// No using namespace in a .h file. Bad to do so
struct Rational{
    int numerator, denominator;
};
// Routine declarations
Rational operator+ (const Rational& a, const Rational& b);
Rational operator+ (const Rational& a, const int& b);

// Other arithmetic operators ...
std::ostream& operator<<(std::ostream& os, const Rational& rat_out);
std::istream& operator>>(std::istream& is, Rational& rat_in);
#endif

