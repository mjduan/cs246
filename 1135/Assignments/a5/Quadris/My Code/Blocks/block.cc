#include <iostream>
#include <string>
#include "block.h"
using namespace std;

//default constructor
Block::Block(){}

// param constructor
Block::Block(int r, int c){
	cout << "Block(int, int) entered" << endl;
	type = 'L';
	x = r; 
	y = c;
	array = new int*[3]; 
	for (int r = 0; r < 3; ++r){
		cout << "r is: " << r << endl; 
		array[r] = new int[3]; 
		for (int c = 0; c < 3; ++c){
			cout << "c is: "<< c << endl;
			array[r][c] = 0; 
		}
	}
	cout << "exiting Block(int,int)" << endl;
}

Block::~Block(){
	for (int r = 0; r < 3; ++r){
		delete [] array[r];		
	}
	delete [] array; 
}

ostream& operator<<(ostream& out, const Block& b){
	int r = 0; 
	for (; r < 3; ++r){
		for (int c = 0; c < 3; ++c){
			out << b.array[r][c];
		}
		out << endl;
	}
	return out;
}

