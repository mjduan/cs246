#include "rational.h"
#include <iostream>
using namespace std;

int main(){
    Rational a = {1, 2};
    Rational b = {7, 5};
    Rational c = a + b;
    Rational d = a + 5;
    int x = 5;
    Rational e = a + x;
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    cout << "c: " << c << endl;
    cout << "d: " << d << endl;
    cout << "e: " << e << endl;
}

