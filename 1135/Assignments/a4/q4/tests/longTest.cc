#include <iostream>
#include <string>
#include <fstream> 
using namespace std; 

int main(){
   
   fstream in;    
   in.open("long.txt");
   string inStr = ""; 
    
   for ( int i = 0; i < 200; i ++) {
      inStr = inStr + "Do Everything ";
   }

   in << inStr << endl; 
   in.close(); 
   
   fstream out; 
   out.open("longTest.out");
   string outStr = "ee^EERYTHIING";
   
   for ( int i = 0; i < 200; i ++) {
      out << outStr << endl; 
   }
   
   out.close(); 
   return 0; 
}
