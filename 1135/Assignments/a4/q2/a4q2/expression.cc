/*******************************************************************
*  CS 246 Assignment 4 Question 2
*
*
*  file: expression.cc
*  Program designed to read and evaluate arithmetic expressions 
********************************************************************/
#include <iostream>
#include <string>
#include "expression.h"
using namespace std; 

//default constructor
Expression::Expression(): 
   infix(""), 
   value(0) {}   

//parameter constructor
Expression::Expression( const string str, long int i ): 
   infix(str), 
   value(i) {}

// copy constructor 
Expression::Expression( const Expression &expr ): 
   infix( expr.infix ), 
   value( expr.value) {}

//defualt destructor
Expression::~Expression(){}

Expression& Expression::operator=(const Expression &rhs){
   if (this == &rhs) return *this; 
   this->infix = rhs.infix;  
   this->value = rhs.value;
   return *this; 
}

std::string Expression::getInfix(){ return infix; }

long int Expression::getValue(){ return value; }

void Expression::setInfix( string str ){ infix = str; }

void Expression::setValue( long int i){ value = i; }

void Expression::prettyprint(){ 
	cout << infix << endl;
	cout << "= " << value << endl;	 
}

void Expression::evaluate() {}





