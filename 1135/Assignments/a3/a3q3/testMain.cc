#include <iostream>
#include <fstream>
#include <string>
#include "test.h"
using namespace std; 

int main (){

   // default constructor test
   //FTree* root = new FTree; 
   //cout << "!--- root name is: " << root->name << endl; 
   //root = ancestor( *root, "adam");
   //cout << root->name << endl; 
   /*
   root->name = "adam"; 
   
   addNode (*root, "adam", "eve1"); 
   addNode (*root, "adam", "eve2"); 
      addNode (*root, "adam", "eve3");
         addNode (*root, "adam", "eve4");
            addNode (*root, "adam", "eve5");
               addNode (*root, "adam", "eve6");
                  addNode (*root, "adam", "eve7");
                     addNode (*root, "adam", "eve8");
                        addNode (*root, "adam", "eve9");
                           addNode (*root, "adam", "eve10");
                              addNode (*root, "adam", "eve11");
     */                        
///**************************************************************
   bool done = false; 
   FTree* root = new FTree; // the root of the entire tree 
   FTree* tempTreePtr = NULL;
   int flag; // flag for keeping track of inStream 
                 // 0 = Stdin
                 // 1 = fileStream
   
   istream *input = &cin;  //sets default input to cin
   flag = 0; 
   ifstream fIn; 
   
   while (!done){
      char c;
      string parent, child, command, file, path, ret;     
      //cout << "!----command?";
      *input >> c; // reads in !, +, ?, 
      
      //if eof reached 
      if ( (*input).eof() && flag == 1 ) {// and instream is filestream
         input = &cin; // switches instream to cin
         
      } else if ((*input).eof() && flag == 0) {// and istream is stdin
         done = true; // exit program
         break;      
      }
      
      switch(c) {
         case 'p':
              print( *root );
              break;        
         
         case '+':
           *input >> parent >> child;    
           if ( !addNode(*root, parent, child) ) {
               cout << "Failed" << endl;
           }
           break;
         
         case '!':
           *input >> parent; 
           if (root->name == " ") {
               root->name = parent; 
               break; 
           } else {
               root = ancestor( *root, parent ); 
           }
           break;  
         case '?':
           ret = " ";
           *input >> child; 
           path = nodePath(*root, child, ret);
           
           if (path == " "){
               cout << "Not found" << endl;
           } else {
               cout << path << endl;
           }                      
           break;
         case '*':
           *input >> parent; 
           delNode( *root, parent ); 
           break;               
         case 'i':
           *input >> command;           
           if (command == "nclude"){
               *input >> file; // reads in file                
               fIn.open("file"); // open the file
               input = &fIn; // set input to fin
               flag = 1; // flag set to fileStream               
               break;     
         }
         break;

      }
   }                  
   delete root; 
	return 0; 
}


