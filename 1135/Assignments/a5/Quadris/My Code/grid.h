#ifndef __GRID_H__
#define __GRID_H__
#include <iostream>
#include "cell.h"
#include "textdisplay.h"

const int maxRows = 18; 
const int maxCols = 10; 


class Grid {
	Cell **theGrid;  // The actual grid. 
	void clearGrid();   // Frees the grid. 
	TextDisplay *td; // The text display.
	Factory *factory; 
	// Add private members, if necessary.
	void calcNeighbour (Cell* cell, const int &r, const int &c);

	// Factory* level;// pointer to factory object
 public:
	Grid();
	~Grid();

	void init(); // Sets up an n x n grid.  Clears old grid, if necessary.
	void tick();      // Runs one iteration of the simulation.
	void setFill(int r, int c, char ch);  // Sets cell at row r, col c to ch.
	friend std::ostream& operator<<(std::ostream &out, const Grid &g);
	void checkClear(); // checks all rows to see if clearable 	
	void makeBlock(); 
};

std::ostream& operator<<(std::ostream &out, const Grid &g);

#endif
