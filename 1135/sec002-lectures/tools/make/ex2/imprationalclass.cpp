#include "imprationalclass.h"

impRational::impRational() : numCalls(0){}
double impRational::toDouble() { numCalls += 1; return Rational::toDouble();}
int impRational::calls(){ return numCalls;}
