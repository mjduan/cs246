#ifndef __LEVEL2_H__
#define __LEVEL2_H__

#include <iostream>
#include "factory.h"

//class Block;

class Level2: public Factory{

	public: 
		Block* makeBlock();	

};

#endif 
