/**********************************************************************
*  CS 246 Assignment 5
*
*
*  file: grid.cc
*  Definitions for methods in Grid class 
************************************************************************/

#include <iostream>
#include "cell.h"
#include "grid.h"
#include "textdisplay.h"
using namespace std; 

// Default Constructor
Grid::Grid(): 
   theGrid(NULL), 
   //gridSize(0), 
   td(NULL) {}

// Destructor
Grid::~Grid(){
	// deletes theGrid
   for (int r = 0; r < rows; ++r){
      // delete each column array
      delete [] theGrid[r]; 
   }
   // detele the row array
   delete [] theGrid; 
  
   // deletes TextDisplay, calling its destructor 
   delete td;   
}

// Frees the grid 
void Grid::clearGrid(){
   //cout << "!----clear grid called:" << endl;
	for (int r = 0; r < rows; ++r){
		//cout << "column " << r << "deleted" << endl;
      delete [] theGrid[r]; // delete each column array
   }
   delete [] theGrid; // delete the row array
	if ( td != NULL ) { delete td; }	
}    

// calculates the neighbours of a given cell based on it's position
//    in the grid and the size of the grid. 
void Grid::calcNeighbour (Cell* cell, const int &r, const int &c, const int &size){
   int x, y; 
   for ( int i = -1; i <= 1; ++i ) {
      for ( int j = -1; j <= 1; ++j ) {
         x = r + i; 
         y = c + j; 
         // if coordinates is self 
         if (x == r && y == c) { continue; }
         // if coordinates is out of bounds 
         if ( x < 0 || y < 0 || x >= rows || y >= cols ){ continue; }

         else {
            cell->addNeighbour( &theGrid[x][y] ); 
         }
      }
   }
}

// Sets up an n x n grid.  Clears old grid, if necessary.
void Grid::newGrid(){
	 
   if ( theGrid ) { // if prev grid exists
     clearGrid();
   }   

   // creates new Text Display 
   td = new TextDisplay(); 

   // creates new grid
   theGrid = new Cell*[n]; 
   for ( int r = 0; r < n; ++r ){// init each column vector       
      theGrid[r] = new Cell[n]; // columns, array of cells  
	}
   
   // Informs each cell of it's neighbours
   for ( int r = 0; r < n; ++r ){
      for ( int c = 0; c < n; ++c ){
			theGrid[r][c].setCoords(r, c);
         calcNeighbour( &theGrid[r][c], r, c, n );      
      }
   }   
}

// Runs one iteration of the simulation.
void Grid::tick(){
	//cout << "tick called" << endl;
/*
   // Step 1) calls each cell's notifyNeighbours
   for ( int r = 0; r < gridSize; ++r ){
      for ( int c = 0; c < gridSize; ++c ) {
 			//cout << "!====== cell " << r << " x " << c << endl; 
         theGrid[r][c].notifyNeighbours();
			//cout << "!--- neighbours notified" << endl;  
      } 
   }

   // Step 2) calls each cell's recalculate method
   for ( int r = 0; r < gridSize; ++r ){
      for ( int c = 0; c < gridSize; ++c ) {
        	//cout << "!====== cell " << r << " x " << c << endl;	
			theGrid[r][c].recalculate();
			//cout << "!--- recalculate finished" << endl; 
         // and notifies the text display
         theGrid[r][c].notifyDisplay(*td);
			//cout << "!---notifyDisplay finished" << endl; 
      } 
   }
*/ 
}      

// Sets fill for cell at row r, col c.
void Grid::setFill( int r, int c, char fill ){
	if (r >= rows || c >= cols) return;
	//cout << "!====== turnOn called for cell"<< r << " x " << c << endl; 
	theGrid[r][c].setFill();
	//cout << "!------ setLiving finished" << endl;
	theGrid[r][c].notifyDisplay(*td);  
	//cout << "!------ notifyDisplay finished" << endl; 
}  

ostream& operator<<(ostream &out, const Grid &g){
	out << *(g.td);
	return out; 
}
