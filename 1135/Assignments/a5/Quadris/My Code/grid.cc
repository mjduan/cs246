/**********************************************************************
*  CS 246 Assignment 4 Question 3
*
*
*  file: grid.cc
*  Definitions for methods in Grid class 
************************************************************************/

#include <iostream>
#include "cell.h"
#include "grid.h"
#include "textdisplay.h"
using namespace std; 

// Default Constructor
Grid::Grid(): 
   theGrid(NULL), 
   //gridSize(0), 
   td(NULL) {}

// Destructor
Grid::~Grid(){
	// deletes theGrid
   for (int r = 0; r < maxRows; ++r){
      // delete each column array
      delete [] theGrid[r]; 
   }
   // detele the row array
   delete [] theGrid; 
  
   // deletes TextDisplay, calling its destructor 
   delete td;   
}

// Frees the grid 
void Grid::clearGrid(){
   //cout << "!----clear grid called:" << endl;
	for (int r = 0; r < maxRows; ++r){
		//cout << "column " << r << "deleted" << endl;
      delete [] theGrid[r]; // delete each column array
   }
   delete [] theGrid; // delete the row array
	if ( td != NULL ) { delete td; }	
}    

// calculates the neighbours of a given cell based on it's position
//    in the grid and the size of the grid. 
void Grid::calcNeighbour (Cell* cell, const int &r, const int &c){
   int x, y; 
   for ( int i = -1; i <= 1; ++i ) {
      for ( int j = -1; j <= 1; ++j ) {
         x = r + i; 
         y = c + j; 
         // if coordinates is self 
         if (x == r && y == c) { continue; }
         // if coordinates is out of bounds 
         if ( x < 0 || y < 0 || x >= maxRows || y >= maxCols ){ continue; }

         else {
            cell->addNeighbour( &theGrid[x][y] ); 
         }
      }
   }
}

// Sets up an n x n grid.  Clears old grid, if necessary.
void Grid::init(){
	 
   if ( theGrid ) { // if prev grid exists
     clearGrid();
   }   
	//gridSize = n;
   // creates new Text Display of size n 
   td = new TextDisplay(); 

   // creates new grid
   theGrid = new Cell*[maxRows]; 
   for ( int r = 0; r < maxRows; ++r ){// init each column vector       
      theGrid[r] = new Cell[maxCols]; // columns, array of cells  
	}
   
   // Informs each cell of it's neighbours
   for ( int r = 0; r < maxRows; ++r ){
      for ( int c = 0; c < maxCols; ++c ){
			theGrid[r][c].setCoords(r, c);
         calcNeighbour( &theGrid[r][c], r, c );      
      }
   }   
}

// Runs one iteration of the simulation.
void Grid::tick(){
	cout << "tick called" << endl;

   // Step 1) calls each cell's notifyNeighbours
   for ( int r = 0; r < maxRows; ++r ){
      for ( int c = 0; c < maxCols; ++c ) {
 			cout << "!====== cell " << r << " x " << c << endl; 
         theGrid[r][c].notifyNeighbours();
			cout << "!--- neighbours notified" << endl;  
      } 
   }

   // Step 2) calls each cell's recalculate method
   for ( int r = 0; r < maxRows; ++r ){
      for ( int c = 0; c < maxCols; ++c ) {
        	cout << "!====== cell " << r << " x " << c << endl;	
			theGrid[r][c].recalculate();
			cout << "!--- recalculate finished" << endl; 
         // and notifies the text display
         theGrid[r][c].notifyDisplay(*td);
			cout << "!---notifyDisplay finished" << endl; 
      } 
   }
}      

// Sets the fill of cell at row 'r', col 'c', to 'ch'.
void Grid::setFill( int r, int c, char ch ){
	if (r >= maxRows || c >= maxCols) return;
	//cout << "!====== turnOn called for cell"<< r << " x " << c << endl; 
	theGrid[r][c].setFill(ch);
	//cout << "!------ setLiving finished" << endl;
	theGrid[r][c].notifyDisplay(*td);  
	//cout << "!------ notifyDisplay finished" << endl; 
}  

void Grid::checkClear(){
	bool filled = true; 

	for (int r = 3; r < maxRows; ++r){
		for (int c = 0; c < maxCols; ++c){
			// if Cell is empty, filled is false
			if (theGrid[r][c].getFill() == NULL) { filled = false; }
		}
		// if all Cells in row filled, clear row
		if (filled) {
			for (int c = 0; c < maxCols; ++c){
				theGrid[r][c].clear();  // calls clear on every cell in filled row
				theGrid[r][c-1].drop();
			}
		}
		filled = true; 
	}
}

void makeBlock(){
	factory->makeBlock; 
}


ostream& operator<<(ostream &out, const Grid &g){
	out << *(g.td);
	return out; 
}
