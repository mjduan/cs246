#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main(){
  ifstream infile("infile1");
  if (infile.fail()){
    cerr << "infile1 is unable to be opened. Dying" << endl;
    return 1;  
  }
  string tmp;
  unsigned int word_count = 0; // Unsigned specifies only non-negative numbers
  while ( infile >> tmp){
    cout << "Word " << word_count << " of infile1 is: " << tmp << endl;
    word_count += 1;
  }

  infile.close();
  infile.open("filenames");
  if (infile.fail()){
    cerr << "Unable to open filenames. Dying" << endl;
    return 2;
  }

  ofstream outfile("filestats");
  if (outfile.fail()){
    cerr << "Unable to open filestats. Dying" << endl;
    return 3;
  }
  outfile << boolalpha;
  while (getline(infile, tmp)){
    ifstream file(tmp.c_str()); // Get a char * representation of the string
    outfile << "Successfully opened " << tmp << ": " << ! file.fail() << endl;
  }
  outfile.close();
  infile.close();

  infile.open("foo");
  outfile.open("bar");
  // Should check to make sure open was successful
  int in_num;
  double in_float;
  for(;;){
    infile >> in_num >> in_float;
    if ( infile.fail() ){
      if(infile.eof()) break;
      infile.ignore();
      infile.clear();
    }
    outfile << in_float * in_num << endl;
  }
  infile.close();
  outfile.close();
}
