#ifndef __ARTICLE_H__
#define __ARTICLE_H__
#include <string>
struct Article{
    std::string name, url, summary;  
};
#endif
