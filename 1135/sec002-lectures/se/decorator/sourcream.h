#ifndef __SOURCREAM_H__
#define __SOURCREAM_H__
#include "tacodecorator.h"


class SourCream : public TacoDecorator{
  public:
    SourCream(Taco& t);
    double cost(); 
    void desc();
};
#endif
