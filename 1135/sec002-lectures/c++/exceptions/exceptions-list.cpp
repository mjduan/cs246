#include <exception>
#include <iostream>
using namespace std;

void rtn() throw(){
   throw 42;
}
void rtn2() throw (float){
   throw 42;
}
void rtn3(){
   throw exception();
}
void rtn4() throw (int){
   throw 42;
}

void rtn5(){
   throw 42;
}
int main(){
  try{
    #if except == 1
       rtn();
    #elif except == 2
       rtn2();
    #elif except == 3
       rtn3();
    #elif except == 4
       rtn4();
    #elif except == 5
       rtn5();
    #endif

  } catch (int e){
    cout << "caught" << endl;
  }

}
