#include <iostream>
using namespace std;

struct Rational{
    int numerator, denominator;
};

istream& operator>>(istream& is, Rational &in_rational){
    is >> in_rational.numerator >> in_rational.denominator;
    return is;
}
ostream& operator<<(ostream& os, const Rational &out_rational){
    os << out_rational.numerator << "/" <<  out_rational.denominator;
    return os;
}
int main(){
    Rational my_num;
    cin >> my_num;
    cout << "Rational number is: " << my_num << endl;
}

