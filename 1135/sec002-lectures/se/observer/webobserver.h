#ifndef __WEBOBSERVER_H__
#define __WEBOBSERVER_H__
#include "article.h"

struct WebObserver{
    virtual void notify(Article a)=0;
    virtual ~WebObserver(){}
};
#endif
