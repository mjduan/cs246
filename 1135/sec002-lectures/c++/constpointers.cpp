#include <iostream>

int main(){

    const int x = 42;
    int y = 42;
    const int *a = &x;
    int * const b = &y;
    int * c = &y;
    const int * const d = &x;
    /*
    x = 7; // Not Allowed
    y = 32; // Allowed
    a=b; //Allowed
    *a = 43; // Not Allowed
    b=c; // Not Allowed
    *b = 43; // Allowed
    c=b; // Allowed
    *c = 1; // Allowed
    d = a; // Not Allowed
    *d = 1; // Not Allowed
    */
}
