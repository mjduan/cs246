#ifndef __GUAC_H__
#define __GUAC_H__
#include "tacodecorator.h"


class Guac : public TacoDecorator{
  public:
    Guac(Taco& t);
    double cost(); 
    void desc();
};
#endif
