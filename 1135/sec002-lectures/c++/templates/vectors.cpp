#include <vector>
#include <iostream>
using namespace std;

int main(){
int i, elem;
vector<int> v;
for(;;){
    cin >> elem;
    if(cin.fail())break;
    v.push_back(elem);
}
vector<int> c;
c = v;
for(i = 0; i < c.size(); ++i) cout << c.at(i) << " "; // subscript checking
cout << endl;
for(i = 0; i < v.size(); ++i) cout << v[i] << " "; // no subscript checking
cout << endl;
v.clear();

for(i=0; i < 5; ++i) v.push_back(2*i);
v.erase(v.begin() + 3);

for(i=0; i < 5 && v[i] != 4; ++i);
v.insert(v.begin() + i, 42); //insert 42 before 4

vector<int>::iterator it;
for(it = v.begin(); it != v.end(); it++) cout << *it << endl;


}
