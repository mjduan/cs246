#include <iostream>
#include <string>
#include <sstream>
#include "textprocess.h"
#include "doublevowels.h"
#include "decorator.h"
using namespace std;

// parameter constructor
DoubleVowels::DoubleVowels(TextProcessor *tp, std::istream* in){
	component = tp;
	source = in;
	component->setSource(in); 
}

// destructor
DoubleVowels::~DoubleVowels(){
	delete component;
} 

// setter for source field
void DoubleVowels::setSource(std::istream *inp){
	source = inp;
}

string DoubleVowels::getWord(){
	string s;
	s = component->getWord();
	
	//cout << "s before: " << s << endl;
	for (unsigned int i = 0; i < s.size(); ++i){
		if ( s[i] == 'a'||s[i] == 'A'||s[i] == 'e'||s[i] == 'E'||s[i] == 'i'
			||s[i] == 'I'||s[i] == 'o'||s[i] == 'O'||s[i] == 'u'||s[i] == 'U' ){
			//cout << "vowel encountered" << endl;
			char copy = s[i];	
			//cout << "vowel is: " << copy << endl; 
			s.insert( i,1,copy );
			i++;
		}
	}
	//cout << "s after: " << s << endl;
	failed = source->fail();
	return s;
}

bool DoubleVowels::fail() const{
	return failed;
}

