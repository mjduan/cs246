#ifndef __LEVEL0_H__
#define __LEVEL0_H__

#include <iostream>
#include <string>
#include <sstream>
#include "factory.h"

class Level0: public Factory{
	std::stringstream sequence; 
	Level0(); 
	~Level0(); 
	//Level0& operator=(istream& in, Level0& ); 
	public:	
		Block* makeBlock();	
	
};


#endif
