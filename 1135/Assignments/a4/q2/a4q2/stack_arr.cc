#include <cstdlib>
#include <iostream>
#include <locale>
#include <string>
#include "stack_arr.h"
#include "expression.h"
using namespace std; 

// implementation of heapbased stack of Expression* using arrays 

//creates a new stack structure
stack* newStack(){
   stack* retStack = new stack; 
   retStack->size = 0; 
   return retStack;
}

// push: Stack int --> Stack
// pushes int onto the stack object
void push(stack* s, Expression* item){
   if (s->size >=10){
      cerr << "Stack overflow" << endl;  
      exit (EXIT_FAILURE); 
   }
   s->data[s->size] = item; 
   s->size++; 
}

// pop: Stack --> Expression*
// removes the top most value on the stack and returns it
Expression* pop( stack* s ){
   Expression* ret; 
   
   if (s->size == 0){
        //cout << "the stack is empty" << endl; 
        return NULL; 
        
   }
   ret = s->data[s->size-1];
   s->size--; 
   return ret;
} 

// top:: Stack --> Expression*
// gives the top value of the stack without poping it
Expression* top( stack* s) {
   if (s->size == 0){
     //cout << "the stack is empty" << endl; 
   }
   
   return s->data[s->size-1];
}



// delStack: Stack --> Void
// Deletes the stack from the heap
void delStack( stack* s) {
   for (int i = 0; i < s->size; ++i){
		delete s->data[s->size];
	} 
   delete s; 
}

// printStack: Stack --> Void
// prints contents of stack to stdout
void printStack( stack* s ) {
   if ( s->size == 0 ){
      cout << "stack empty" << endl;      
   }

   cout << "STACK_START" << endl; 
   for ( int i = s->size - 1; i >= 0; i--){
      cout << s->data[i]->getValue() <<" | " <<s->data[i] << endl; 
   }
   cout << "STACK___END" << endl; 
}

/*
// peek: Stack --> Expression*
// gives the value of the second item on the stack
bool peek ( stack* s ) {
	if ( s->size == 0 || s->size == 1 ){
		//cout << "the stack is empty" << endl;
		return false;
   } else {
	   return true; 
	}      
}
*/

