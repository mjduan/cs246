#if DEBUG == 1
    #include "debug1.h"
#elif DEBUG == 2
    #include "debug2.h"
#else 
    #include <iostream>
    using namespace std;
    void errFunc(int code){
        cerr << "No errors here" << endl;
    }
#endif


int main(){
    errFunc(1);
    errFunc(2);
    errFunc(3);
}
