#include <iostream>
#include <string>
#include <sstream>
#include "textprocess.h"
#include "skipword.h"
using namespace std;

SkipWord::SkipWord(TextProcessor *tp, istream *in){
	source = in; 
	component = tp;
	component->setSource(in);
	counter = 0;
}

// destructor
SkipWord::~SkipWord(){
   delete component;
}

// source setter
void SkipWord::setSource(std::istream *inp){
	source = inp;	
}

string SkipWord::getWord(){
	unsigned int counter = 0;
	string s; 
	s = component->getWord(); 
	counter++;
	cout << "!---counter is: " <<counter << endl; 
	//if count is even return word, else return empty string
	return ((counter %2) == 0 ) ? s :  ""; 
}

bool SkipWord::fail() const{
	return failed;
}

