#include <iostream>
using namespace std;

struct Bar{
    virtual void r(){ cout << "Bar::r" << endl;}
};

struct Foo : public Bar{
    void r(){cout << "Foo::r" << endl;}

};

void rtn (Bar& rb) {rb.r();}

int main(){
    Foo f;
    Bar b;
    rtn(f);
    Bar & rb = f;
    rb.r();


}
