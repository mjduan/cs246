#ifndef __LEVEL3_H__
#define __LEVEL3_H__

#include <iostream>
#include "factory.h"

//class Block;

class Level3: public Factory{

	public: 
		Block* makeBlock();	

};

#endif
