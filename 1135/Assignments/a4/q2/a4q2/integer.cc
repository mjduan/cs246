/*******************************************************************
*  CS 246 Assignment 4 Question 2
*
*
*  file: integer.cc
*  Definitions for methods in Integer class, derived class of Expression 
********************************************************************/

#include <string>
#include <iostream>
#include <sstream>
#include "integer.h"
using namespace std; 

// Default constructor 
Integer::Integer():
   Expression() {}

// Parameter constructor 
Integer::Integer( const long int i ):
   Expression( intToString(i), i ) {}

// copy constructor                                                    
Integer::Integer( const Integer& target ):
   Expression(target) {} 

Integer& Integer::operator=(const Integer &rhs){
   if (this == &rhs) return *this; 
   Expression::operator=(rhs);
   return *this; 
}

void Integer::prettyprint(){
   cout << getInfix() << endl;
   cout << "= " << getValue() << endl;
}

void Integer::evaluate(){}

string intToString (int num) {
   stringstream ss; 
   ss << num; 
   return ss.str(); 
}
