#ifndef __DOUBLEVOWELS_H__
#define __DOUBLEVOWELS_H__
#include <iostream>
#include <string>
#include "textprocess.h"
#include "decorator.h"

class DoubleVowels: public Decorator {
		std::istream *source;
		bool failed;
	public:
		DoubleVowels(TextProcessor *tp, std::istream* in);
		~DoubleVowels(); // destructor
		void setSource(std::istream *inp);
		std::string getWord();
		bool fail() const;

};

#endif
