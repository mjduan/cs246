#include <iostream>
using namespace std;
struct FancyStruct{
    const int i;
    int &j;
    int k;
    FancyStruct(int p1, int &p2, int p3): i(p1), j(p2), k(p3){}
};

/*struct BadStruct{
    const int i;
    int &j;
    BadStruct(int p1, int &p2){
      i = p1;
      j = p2;
    }
};*/

int main(){
    int i = 42;
    int j = 43;
    FancyStruct f(i,j,44);
    cout << f.i << " " << f.j << " " << f.k <<  endl;
//    BadStruct b(i,j);
}
