#include <iostream>
using namespace std;
struct Rational{
    int numerator, denominator;
    /*Rational(int n = 0, int d = 1) {
        numerator = n;
        denominator = (d != 0) ? d : 1;
    }
    Rational(const Rational& b){
        numerator = b.numerator;
        denominator = b.denominator;   
    }*/
    double toDouble() const{
        return (double)numerator/denominator;
    }
};

double toDouble(const Rational& a){
    return (double)a.numerator/a.denominator;
}
int main(){
    Rational r = {1, 2};
    cout << r.toDouble() << endl;
    cout << toDouble(r) << endl;

}
