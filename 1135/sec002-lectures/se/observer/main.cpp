#include "gamereviewer.h"
#include "rssfeed.h"
#include "emailfeed.h"
#include <iostream>

int main(){
    std::cout << std::endl << std::endl;
    GameReviewer ign("ign.com");
    RSSFeed googleReader;
    EmailFeed mywaterloo;
    googleReader.sub(&ign);
    ign.attach(mywaterloo);
    ign.review("Borderlands 2: Tiny Tina's Assault on Dragon's Keep");
    ign.detach(mywaterloo);
    ign.review("Deadpool");
    googleReader.unsub(&ign);
    ign.review("ChamberCrawler3000");
}
