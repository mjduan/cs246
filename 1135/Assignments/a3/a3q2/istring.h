#ifndef __ISTRING_H__
#define __ISTRING_H__
#include <iostream>

struct iString{
	char * chars;
	unsigned int length;
	unsigned int capacity;
	iString();
	iString(const char * str);
	iString(const iString& iS);
	~iString();
};

// Add other declarations here:

iString operator+(const iString& iS1, const iString& iS2);

iString operator+(const iString& iS, const char* str); 

iString operator*(const iString& iS, const int n);

iString operator*(const int n, const iString& iS);

std::ostream& operator<<(std::ostream& out, const iString& iS);

std::istream& operator>>(std::istream& in, iString& iS);

#endif
