#include <exception>
#include <iostream>
using namespace std;
struct Foo{
  int arr[10];
  ~Foo(){cout << "Foo begone" << endl;}
};
void rtn(){
   Foo f;
   Foo *fp;
   try{
   	fp = new Foo;
	throw exception();
   } catch (...){
     if (fp != NULL) delete fp;
     throw;
   }
}

int main(){
  try{
     rtn();
  } catch (exception e){
     cout << "caught: " << e.what() << endl;
  }
}
